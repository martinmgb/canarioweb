package vistaModelo;

public class TipoCargoFilter {

	private String codigo="", tipo="";

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo==null?"":codigo.trim();
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo==null?"":tipo.trim();
	}


}
