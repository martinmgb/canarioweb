package vistaModelo;

public class SancionFilter {
	private String codigo="", tipo="", fecha="", cedula="", nombre="", codIncidencia="" ;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo==null?"":codigo.trim();
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo==null?"":tipo.trim();
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha==null?"":fecha.trim();
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula==null?"":cedula.trim();
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}
	public String getCodIncidencia() {
		return codIncidencia;
	}

	public void setCodIncidencia(String tipo) {
		this.codIncidencia = codIncidencia==null?"":codIncidencia.trim();
	}
	


}
