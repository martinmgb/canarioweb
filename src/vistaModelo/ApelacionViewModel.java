package vistaModelo;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.collections.map.HashedMap;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Map;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import modelo.entidad.Apelacion;
import modelo.entidad.ApelacionData;
import modelo.entidad.Sancion;
import vistaModelo.ApelacionFilter;

 
public class ApelacionViewModel {
 
    private static final String footerMessage = "Total de %d Apelacion(es) ";
    private ApelacionFilter apelacionFilter = new ApelacionFilter();
    List<Apelacion> currentApelacion = ApelacionData.getAllApelaciones();
 
    public ApelacionFilter getApelacionsFilter() {
        return apelacionFilter;
    }
 
    public ListModel<Apelacion> getApelacionModel() {
        return new ListModelList<Apelacion>(currentApelacion);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentApelacion.size());
    }
 
    @Command
    @NotifyChange({"apelacionModel", "footer"})
    public void changeFilter() {
    	currentApelacion = ApelacionData.getFilterApelaciones(apelacionFilter);
    }
    
    @Command
    public void changeEditable(@BindingParam("apelaciones") Apelacion com) {
    	Sancion s = new Sancion("1","tipo","asd","15/05/2015","23542438", "Martín Gutiérrez", "Incidencia");
    	//ashMap<Sancion, Apelacion> myParams = new HashMap<Sancion, Apelacion>();
    	//myParams.put("objeto", com);
    	//HashedMap apelacion = new HashedMap<String, String>();
    	HashedMap info = new HashedMap();
    	info.put("ape", com);
    	info.put("san", s);
    	Window window = (Window)Executions.createComponents(
				"../content/modalAtenderSolicitudApelacion.zul", null, info);
		window.doModal();
    	}
}