package vistaModelo;

public class RecursoFilter {

	private String codigo="", nombre="",fechaingreso="",cantidad="", descripcion="";

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo==null?"":codigo.trim();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}


	public String getFechaingreso() {
		return fechaingreso;
	}

	public void setFechaingreso(String fechaingreso) {
		this.fechaingreso = fechaingreso==null?"":fechaingreso.trim();
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setcCantidad(String cantidad) {
		this.cantidad = cantidad==null?"":cantidad.trim();
	}


	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion==null?"":descripcion.trim();
	}
}
