package vistaModelo;

import java.util.List;

import org.apache.commons.collections.map.HashedMap;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Map;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import modelo.entidad.TipoParentesco;
import modelo.entidad.TipoParentescoData;
import vistaModelo.TipoParentescoFilter;

 
public class TipoParentescoViewModel {
 
    private static final String footerMessage = "Total de %d parentesco(s) ";
    private TipoParentescoFilter tipoparentescoFilter = new TipoParentescoFilter();
    List<TipoParentesco> currentTipoParentesco = TipoParentescoData.getAllTipoParentescoes();
 
    public TipoParentescoFilter getTiposParentescoFilter() {
        return tipoparentescoFilter;
    }
 
    public ListModel<TipoParentesco> getTipoParentescoModel() {
        return new ListModelList<TipoParentesco>(currentTipoParentesco);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentTipoParentesco.size());
    }
 
    @Command
    @NotifyChange({"tipoParentescoModel", "footer"})
    public void changeFilter() {
    	currentTipoParentesco = TipoParentescoData.getFilterTipoParentescoes(tipoparentescoFilter);
    }
    
    @Command
    public void changeEditable(@BindingParam("tipoparentescos") TipoParentesco com) {
    	HashedMap tipoparentesco = new HashedMap();
    	tipoparentesco.put("objeto", com);
    	Window window = (Window)Executions.createComponents(
				"../content/modalTipoParentesco.zul", null, tipoparentesco);
		window.doModal();
    	}
}