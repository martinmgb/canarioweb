package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import modelo.entidad.TipoArea;
import modelo.entidad.TipoAreaData;

 
public class TipoAreaViewModel {
 
    private static final String footerMessage = "Total de %d Area(s) ";
    private TipoAreaFilter areaFilter = new TipoAreaFilter();
    List<TipoArea> currentArea = TipoAreaData.getAllAreas();
 
    public TipoAreaFilter getAreaFilter() {
        return areaFilter;
    }
 
    public ListModel<TipoArea> getAreaModel() {
        return new ListModelList<TipoArea>(currentArea);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentArea.size());
    }
 
    @Command
    @NotifyChange({"areaModel", "footer"})
    public void changeFilter() {
    	currentArea = TipoAreaData.getFilterAreas(areaFilter);
    }
}