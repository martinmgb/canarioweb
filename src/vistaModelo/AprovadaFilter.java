package vistaModelo;

public class AprovadaFilter {
	private String codigo="";
	private String nombre="";
	private String cedula="";
	private String apellido="";
	private String tipo="";
	private String area="";
	private String fecha="";
	private String monto="";
	private String montopagado="";


	public String getCedula() {
		return cedula;
	}


	public String getTipo() {
		return tipo;
	}

	

	public String getCodigo() {
		return codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getArea() {
		return area;
	}

	public String getFecha() {
		return fecha;
	}

	public String getMonto() {
		return monto;
	}

	public String getMontopagado() {
		return montopagado;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula==null?"":cedula.trim();
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo==null?"":codigo.trim();
	}

	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}
	public void setApellido(String apellido) {
		this.apellido = apellido==null?"":apellido.trim();

	}
	public void setArea(String area) {
		this.area = area==null?"":area.trim();

	}
	public void setFecha(String fecha) {
		this.fecha = fecha==null?"":fecha.trim();

	}
	
	public void setMonto(String monto) {
		this.monto = monto==null?"":monto.trim();

	}
	public void setMontoPagado(String montopa) {
		this.montopagado = montopagado==null?"":montopagado.trim();

	}



}
