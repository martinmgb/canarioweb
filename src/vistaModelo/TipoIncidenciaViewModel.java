package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
 
import modelo.entidad.TipoIncidencia;
import modelo.entidad.TipoIncidenciaData;
 
public class TipoIncidenciaViewModel {
 
    private static final String footerMessageIncidencia = "En Total %d Incidencia(s)";
    
    private TipoIncidenciaFilter filtroIncidencia = new TipoIncidenciaFilter();
    
    

    List<TipoIncidencia> incidenciaActuales = TipoIncidenciaData.getAllIncidencia();
    
 
    
    
    public TipoIncidenciaFilter getFiltroIncidencia() {
        return filtroIncidencia;
    }
 
    public ListModel<TipoIncidencia> getIncidenciaModel() {
        return new ListModelList<TipoIncidencia>(incidenciaActuales);
    }
     
    public String getFooterIncidencia() {
        return String.format(footerMessageIncidencia, incidenciaActuales.size());
    }
    
 
    @Command
    @NotifyChange({"incidenciaModel", "footer"})
    public void changeFilter2() {
        incidenciaActuales = TipoIncidenciaData.getFilterIncidencia(filtroIncidencia);
    }
    
    
}
