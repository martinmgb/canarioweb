package vistaModelo;

import java.util.List;

import modelo.entidad.InsolventeData;
import modelo.entidad.Insolvente;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

public class InsolventeViewModel {
	private  String footerMessage = "En Total %d Insolventes(s)";
    private InsolventeFilter filtroInsolvente = new InsolventeFilter();
  
    List<Insolvente> insolventesActuales = InsolventeData.getTodosInsolventes();
 
    public InsolventeFilter getFiltroInsolvente() {
    	return filtroInsolvente;   	  
    }
 
  //Retorna todos los Insolventes que mandara a la vista
    public ListModel<Insolvente> getInsolventeModel() {
        return new ListModelList<Insolvente>(insolventesActuales);
  
    }
  //Muestra la cantida de elementos
    public String getFooter() {
        return String.format(footerMessage, insolventesActuales.size());
    }
 
    @Command
    @NotifyChange({"insolventeModel", "footer"})
    public void changeFilter() {
    	insolventesActuales = InsolventeData.getFiltroInsolvente(filtroInsolvente);
    }	
    
    

}
