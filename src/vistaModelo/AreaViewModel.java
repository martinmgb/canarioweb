package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
 
import modelo.entidad.AreaData;
import modelo.entidad.Area;
 
public class AreaViewModel {
 
    private static final String footerMessage = "En Total %d Area(s)";
    private AreaFilter filtroArea = new AreaFilter();
    List<Area> areasActuales = AreaData.getTodasAreas();
 
    public AreaFilter getFiltroArea() {
        return filtroArea;
    }
 
    public ListModel<Area> getAreaModel() {
        return new ListModelList<Area>(areasActuales);
    }
     
    public String getFooter() {
        return String.format(footerMessage, areasActuales.size());
    }
 
    @Command
    @NotifyChange({"areaModel", "footer"})
    public void changeFilter() {
        areasActuales = AreaData.getFiltroAreas(filtroArea);
    }
}
