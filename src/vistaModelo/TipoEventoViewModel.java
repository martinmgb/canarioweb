package vistaModelo;

import java.util.List;

import modelo.entidad.TipoEvento;
import modelo.entidad.TipoEventoData;
import modelo.entidad.TipoIndicador;
import modelo.entidad.TipoIndicadorData;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

public class TipoEventoViewModel {
	 private static final String footerMessage = "Total de %d Tipo Evento(s) ";
	    private TipoEventoFilter tipoFilterEvento = new TipoEventoFilter();
	    List<TipoEvento> currentTipo = TipoEventoData.getAllTipos();
	 
	    public TipoEventoFilter gettipoFilter() {
	        return tipoFilterEvento;
	    }
	 
	    public ListModel<TipoEvento> getTipoModel() {
	        return new ListModelList<TipoEvento>(currentTipo);
	    }
	     
	    public String getFooter() {
	        return String.format(footerMessage, currentTipo.size());
	    }
	 
	    @Command
	    @NotifyChange({"tipoModel", "footer"})
	    public void changeFilter() {
	    	currentTipo = TipoEventoData.getFilterTipos(tipoFilterEvento);
	    }
}
