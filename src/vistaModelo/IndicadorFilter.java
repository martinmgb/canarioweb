package vistaModelo;
/*creado por Aura Manzanilla
fecha 14/06/2015*/

public class IndicadorFilter {
	private String codigo="",nombre="", tipo="", unidad="";

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo==null?"":codigo.trim();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo==null?"":tipo.trim();
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad==null?"":unidad.trim();
	}

}
