package vistaModelo;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/
public class SolicitudEventoFilter {

	private String codigo="",nombre="",fecha="",area="";

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo==null?"":codigo.trim();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha==null?"":fecha.trim();
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area==null?"":area.trim();
	}

}