package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import modelo.entidad.Recurso;
import modelo.entidad.RecursoData;

 
public class RecursoViewModel {
 
    private static final String footerMessage = "Total de %d Recurso(s) ";
    private RecursoFilter recursoFilter = new RecursoFilter();
    List<Recurso> currentRecurso = RecursoData.getAllRecursos();
 
    public RecursoFilter getRecursoFilter() {
        return recursoFilter;
    }
 
    public ListModel<Recurso> getRecursoModel() {
        return new ListModelList<Recurso>(currentRecurso);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentRecurso.size());
    }
 
    @Command
    @NotifyChange({"recursoModel", "footer"})
    public void changeFilter() {
    	currentRecurso = RecursoData.getFilterRecursos(recursoFilter);
    }
}