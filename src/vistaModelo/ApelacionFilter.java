package vistaModelo;

import java.util.Date;

public class ApelacionFilter {

	private String codigo="",miembro="", descripcion="" ;
	private Date fecha;

	

	public void setCodigo(String codigo) {
		this.codigo = codigo==null?"":codigo.trim();
	}
	
	public void setMiembro(String miembro) {
		this.miembro = miembro==null?"":miembro.trim();
	}
/*
	public void setFecha(Date fecha) {
		if (fecha==null)
			this.fecha = new Date();
		else
			this.fecha =  fecha.trim();
	}*/
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion==null?"":descripcion.trim();
	}

	public String getMiembro() {
		return miembro;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public Date getFecha() {
		return fecha;
	}

	public String getCodigo() {
		return codigo;
	}
	
	

}