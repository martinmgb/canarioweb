package vistaModelo;

import java.util.List;

import modelo.entidad.Cliente;
import modelo.entidad.ClienteData;
import modelo.entidad.SolicitudesAprovadasData;
import modelo.entidad.SolicitudesAprovadas;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

public class ClienteViewModel {
	private static  String footerMessage = "En Total %d Clientes(s)";
    private ClienteFilter filtroClientes = new ClienteFilter();
    List<Cliente> clientes = ClienteData.getTodosClientes();
 
    public  ClienteFilter  getfiltroClientes() {
        return filtroClientes;
    }
  //Retorna todas las solicitudes que mandara a la vista
    public ListModel<Cliente> getClientesModel() {
        return new ListModelList<Cliente>(clientes);
    }
     
    public String getFooter() {
        return String.format(footerMessage, clientes.size());
    }
    
    @Command
    @NotifyChange({"clientesModel", "footer"})
    public void changeFilter() {
    	clientes = ClienteData.getFiltroCliente(filtroClientes);
    }
    		
	


}
