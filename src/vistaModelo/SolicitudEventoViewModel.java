package vistaModelo;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

import modelo.entidad.SolicitudEvento;
import modelo.entidad.SolicitudEventoData;
import vistaModelo.SolicitudEventoFilter;

 
public class SolicitudEventoViewModel {
 
    private static final String footerMessage = "Total de %d SolicitudEvento(s) ";
    private SolicitudEventoFilter solicitudEventoFilter = new SolicitudEventoFilter();
    List<SolicitudEvento> currentSolicitudEvento = SolicitudEventoData.getAllSolicitudEventos();
 
    public SolicitudEventoFilter getSolicitudEventosFilter() {
        return solicitudEventoFilter;
    }
 
    public ListModel<SolicitudEvento> getSolicitudEventoModel() {
        return new ListModelList<SolicitudEvento>(currentSolicitudEvento);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentSolicitudEvento.size());
    }
 
    @Command
    @NotifyChange({"comisionModel", "footer"})
    public void changeFilter() {
    	currentSolicitudEvento = SolicitudEventoData.getFilterSolicitudEventos(solicitudEventoFilter);
    }

}