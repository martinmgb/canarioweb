package vistaModelo;
/*creado por Aura Manzanilla 19669350
fecha 14/06/2015*/
import java.util.List;

import modelo.entidad.Cargo;
import modelo.entidad.CargoData;

import org.apache.commons.collections.map.HashedMap;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Map;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;


import vistaModelo.CargoFilter;


 
public class CargoViewModel {
 
    private static final String footerMessage = "Total de %d Cargo(s) ";
    private CargoFilter cargoFilter = new CargoFilter();
    List<Cargo> currentCargo = CargoData.getAllCargos();
 
    public CargoFilter getCargosFilter() {
        return cargoFilter;
    }
 
    public ListModel<Cargo> getCargoModel() {
        return new ListModelList<Cargo>(currentCargo);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentCargo.size());
    }
 
    @Command
    @NotifyChange({"cargoModel", "footer"})
    public void changeFilter() {
    	currentCargo = CargoData.getFilterCargos(cargoFilter);
    }
    
    @Command
    public void changeEditable(@BindingParam("cargos") Cargo ca) {
    	HashedMap cargo = new HashedMap();
    	cargo.put("objeto", ca);
    	Window window = (Window)Executions.createComponents(
				"../content/modalCargo.zul", null, cargo);
		window.doModal();
    	}
}