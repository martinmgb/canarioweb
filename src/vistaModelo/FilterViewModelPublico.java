package vistaModelo;



import java.util.List;





import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

import modelo.entidad.DataPublico;
import modelo.entidad.Publico;

public class FilterViewModelPublico {
 
    private static final String footerMessage = "En total %d tipo(s) de público";
    private FiltroPublico filtroPublico = new FiltroPublico();
  ;
    List<Publico> publicoActual = DataPublico.getTodoPublico();
  
 
    public FiltroPublico getFiltroPublico() {
        return filtroPublico;
    }
    
    
 
    public ListModel<Publico> getPublicoModel() {
        return new ListModelList<Publico>(publicoActual);
    }
     
    public String getFooter() {
        return String.format(footerMessage, publicoActual.size());
    }
 
    @Command
    @NotifyChange({"publicoModel", "footer"})
    public void changeFilter() {
        publicoActual = DataPublico.getFiltroPublico(filtroPublico);
    }
}



