package vistaModelo;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/
public class ComisionFilter {

	private String codigo="",nombre="", tipocomision="";

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo==null?"":codigo.trim();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}

	public String getTipocomision() {
		return tipocomision;
	}

	public void setTipocomision(String tipocomision) {
		this.tipocomision = tipocomision==null?"":tipocomision.trim();
	}

}