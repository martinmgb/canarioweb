package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import modelo.entidad.TipoCargo;
import modelo.entidad.TipoCargoData;

 
public class FilterViewModelTipoCargo {
 
    private static final String footerMessage = "Total de %d Cargo(s) ";
    private TipoCargoFilter cargoFilter = new TipoCargoFilter();
    List<TipoCargo> currentCargo = TipoCargoData.getAllCargos();
 
    public TipoCargoFilter getCargoFilter() {
        return cargoFilter;
    }
 
    public ListModel<TipoCargo> getCargoModel() {
        return new ListModelList<TipoCargo>(currentCargo);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentCargo.size());
    }
 
    @Command
    @NotifyChange({"cargoModel", "footer"})
    public void changeFilter() {
    	currentCargo = TipoCargoData.getFilterCargos(cargoFilter);
    }
}