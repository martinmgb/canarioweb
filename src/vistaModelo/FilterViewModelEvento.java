package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

import modelo.entidad.AsistenciaEvento;
import modelo.entidad.AsistenciaEventoData;

 
public class FilterViewModelEvento {
 
    private static final String footerMessage = "Total de %d Evento(s) ";
    private AsistenciaEventoFilter eventoFilter = new AsistenciaEventoFilter();
    List<AsistenciaEvento> currentEvento = AsistenciaEventoData.getAllEvento();
 
    public AsistenciaEventoFilter getEventoFilter() {
        return eventoFilter;
    }
 
    
    
    public ListModel<AsistenciaEvento> getEventoModel() {
        return new ListModelList<AsistenciaEvento>(currentEvento);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentEvento.size());
    }
 
    @Command
    @NotifyChange({"eventoModel", "footer"})
    public void changeFilter() {
    	currentEvento = AsistenciaEventoData.getFilterEvento(eventoFilter);
    }
}