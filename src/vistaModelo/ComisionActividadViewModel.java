package vistaModelo;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/
import java.util.List;

import org.apache.commons.collections.map.HashedMap;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import modelo.entidad.ComisionActividad;
import modelo.entidad.ComisionActividadData;
import vistaModelo.ComisionActividadFilter;

 
public class ComisionActividadViewModel {
 
    private static final String footerMessage = "Total de %d Actividad(s) por Comision ";
    private ComisionActividadFilter comisionActividadFilter = new ComisionActividadFilter();
    List<ComisionActividad> currentComisionActividad = ComisionActividadData.getAllComAct();
 
    public ComisionActividadFilter getComisionActividadFilter() {
        return comisionActividadFilter;
    }
 
    public ListModel<ComisionActividad> getComisionActividadModel() {
        return new ListModelList<ComisionActividad>(currentComisionActividad);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentComisionActividad.size());
    }
 
    @Command
    @NotifyChange({"comisionActividadModel", "footer"})
    public void changeFilter() {
//    	currentComisionActividad = ComisionActividadData.getFilterComisionActividades(comisionActividadFilter);
    }
    
    @Command
    public void changeEditable(@BindingParam("comisiones") ComisionActividad com) {
    	HashedMap comision = new HashedMap();
    	comision.put("objeto", com);
    	Window window = (Window)Executions.createComponents(
				"../content/modalComisionActividad.zul", null, comision);
		window.doModal();
    	}
}