package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import modelo.entidad.Actividad;
import modelo.entidad.ActividadData;;

 
public class FilterViewModelActividad {
 
    private static final String footerMessage = "Total de %d Actividad(s) ";
    private ActividadFilter actividadFilter = new ActividadFilter();
    List<Actividad> currentActividad = ActividadData.getAllActividades();
 
    public ActividadFilter getActividadFilter() {
        return actividadFilter;
    }
 
    public ListModel<Actividad> getActividadModel() {
        return new ListModelList<Actividad>(currentActividad);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentActividad.size());
    }
 
    @Command
    @NotifyChange({"actividadModel", "footer"})
    public void changeFilter() {
    	currentActividad = ActividadData.getFilterActividades(actividadFilter);
    }
}