package vistaModelo;



import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

import modelo.entidad.DataTDifusion;
import modelo.entidad.TDifusion;
 
public class FilterViewModelTDifusion {
 
    private static final String footerMessage = "En total %d tipo(s) de difusión";
    private FiltroTDifusion filtroTDifusion = new FiltroTDifusion();
  ;
    List<TDifusion> difusionesActuales = DataTDifusion.getTodasTDifusion();
  
 
    public FiltroTDifusion getFiltroTDifusion() {
        return filtroTDifusion;
    }
    
    
 
    public ListModel<TDifusion> getTDifusionModel() {
        return new ListModelList<TDifusion>(difusionesActuales);
    }
     
    public String getFooter() {
        return String.format(footerMessage, difusionesActuales.size());
    }
 
    @Command
    @NotifyChange({"TDifusionModel", "footer"})
    public void changeFilter() {
        difusionesActuales = DataTDifusion.getFiltroTDifusion(filtroTDifusion);
    }
}



