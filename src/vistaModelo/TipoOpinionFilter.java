package vistaModelo;

import java.util.List;

import modelo.entidad.TipoOpinion;
import modelo.entidad.TipoOpinionData;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

 
public class TipoOpinionFilter {
 
    private static final String footerMessage = "Total de %d Opiniones(s) ";
    private TipoOpinionViewModel opinionFilter = new TipoOpinionViewModel();
    List<TipoOpinion> currentOpinion = TipoOpinionData.getAllOpiniones();
 
    public TipoOpinionViewModel getOpinionFilter() {
        return opinionFilter;
    }
 
    public ListModel<TipoOpinion> getOpinionModel() {
        return new ListModelList<TipoOpinion>(currentOpinion);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentOpinion.size());
    }
 
    @Command
    @NotifyChange({"opinionModel", "footer"})
    public void changeFilter() {
    	currentOpinion = TipoOpinionData.getFilterOpiniones(opinionFilter);
    }
}