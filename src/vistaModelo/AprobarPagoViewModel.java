package vistaModelo;

import java.util.List;

import modelo.entidad.AprobarPagoArrendamientoData;
import modelo.entidad.AprobarPagoArrendamiento;
import modelo.entidad.TipoEvento;
import modelo.entidad.TipoEventoData;
import modelo.entidad.TipoIndicador;
import modelo.entidad.TipoIndicadorData;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

public class AprobarPagoViewModel {
	 private static final String footerMessage = "Total de %d Tipo Evento(s) ";
	    private TipoEventoFilter tipoFilterEvento = new TipoEventoFilter();
	    List<AprobarPagoArrendamiento> currentTipo = AprobarPagoArrendamientoData.getAllOpiniones();
	 
	    public TipoEventoFilter gettipoFilter() {
	        return tipoFilterEvento;
	    }
	 
	    public ListModel<AprobarPagoArrendamiento> getaprobarModel() {
	        return new ListModelList<AprobarPagoArrendamiento>(currentTipo);
	    }
	     
	    public String getFooter() {
	        return String.format(footerMessage, currentTipo.size());
	    }
	 
	   /* @Command
	    @NotifyChange({"tipoModel", "footer"})
	    public void changeFilter() {
	    	currentTipo = TipoEventoData.getFilterTipos(tipoFilterEvento);
	    }*/
}