package vistaModelo;

import java.util.List;

import modelo.entidad.InsolventeData;
import modelo.entidad.SolicitudesAprovadasData;
import modelo.entidad.Insolvente;
import modelo.entidad.SolicitudesAprovadas;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

public class AprovadasViewModel {

	private static  String footerMessage = "En Total %d Solicitudes Aprovados(s)";
    private AprovadaFilter filtroAprovadas = new AprovadaFilter();
    List<SolicitudesAprovadas> solicitudesaprovadas = SolicitudesAprovadasData.getTodasSolicitudesAprovadas();
 
    public  AprovadaFilter  getfiltroAprovadas() {
        return filtroAprovadas;
    }
  //Retorna todas las solicitudes que mandara a la vista
    public ListModel<SolicitudesAprovadas> getAprovadasModel() {
        return new ListModelList<SolicitudesAprovadas>(solicitudesaprovadas);
    }
     
    public String getFooter() {
        return String.format(footerMessage, solicitudesaprovadas.size());
    }
    
    @Command
    @NotifyChange({"aprovadasModel", "footer"})
    public void changeFilter() {
    	solicitudesaprovadas = SolicitudesAprovadasData.getFilterSolicitudes(filtroAprovadas);
    }
    		
	
}
