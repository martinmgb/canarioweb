package vistaModelo;
/*creado por Aura Manzanilla
fecha 14/06/2015*/
import java.util.List;

import org.apache.commons.collections.map.HashedMap;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import modelo.entidad.Indicador;
import modelo.entidad.IndicadorData;
import modelo.entidad.TipoNoticia;

 
public class IndicadorViewModel {
 
    private static final String footerMessage = "Total de %d Indicador(s) ";
    private IndicadorFilter indicadorFilter = new IndicadorFilter();
    List<Indicador> currentIndicador = IndicadorData.getAllIndicadores();
 
    public IndicadorFilter getIndicadorsFilter() {
        return indicadorFilter;
    }
 
    public ListModel<Indicador> getIndicadorModel() {
        return new ListModelList<Indicador>(currentIndicador);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentIndicador.size());
    }
 
    @Command
    @NotifyChange({"indicadorModel", "footer"})
    public void changeFilter() {
    	currentIndicador = IndicadorData.getFilterIndicadores(indicadorFilter);
    }
    
    @Command
    public void changeEditable(@BindingParam("ind") Indicador ind) {
    	HashedMap obj = new HashedMap();
    	obj.put("objeto", ind);
    	Window window = (Window)Executions.createComponents(
				"../content/modalIndicador.zul", null, obj);
		window.doModal();
    	}

}