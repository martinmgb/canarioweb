package vistaModelo;

import java.util.List;

import modelo.entidad.Cliente;
import modelo.entidad.ClienteData;
import modelo.entidad.PagoArrendamientoData;
import modelo.entidad.PagoArrendamiento;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

public class PagoArrendamientoViewModel {
	private static  String footerMessage = "En Total %d Pagos(s)";
    private PagoArrendamientoFilter filtroPagos = new PagoArrendamientoFilter();
    List<PagoArrendamiento> pagos = PagoArrendamientoData.getAllPagosArrendamiento();
 
    public  PagoArrendamientoFilter  getfiltroPagos() {
        return filtroPagos;
    }
  //Retorna todas los pagos que mandara a la vista
    public ListModel<PagoArrendamiento> getPagosModel() {
        return new ListModelList<PagoArrendamiento>(pagos);
    }
     
    public String getFooter() {
        return String.format(footerMessage, pagos.size());
    }
    
    @Command
    @NotifyChange({"pagosModel", "footer"})
    public void changeFilter() {
    	pagos = PagoArrendamientoData.getFilterPagosArrendamiento(filtroPagos);
    }
	
}
