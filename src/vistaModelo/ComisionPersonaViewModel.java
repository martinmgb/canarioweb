package vistaModelo;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/
import java.util.List;

import org.apache.commons.collections.map.HashedMap;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import modelo.entidad.ComisionPersona;
import modelo.entidad.ComisionPersonaData;
import vistaModelo.ComisionPersonaFilter;

 
public class ComisionPersonaViewModel {
 
    private static final String footerMessage = "Total de %d Persona(s) por Comision";
    private ComisionPersonaFilter comisionPersonaFilter = new ComisionPersonaFilter();
    List<ComisionPersona> currentComisionPersona = ComisionPersonaData.getAllComPer();
 
    public ComisionPersonaFilter getComisionPersonaFilter() {
        return comisionPersonaFilter;
    }
 
    public ListModel<ComisionPersona> getComisionPersonaModel() {
        return new ListModelList<ComisionPersona>(currentComisionPersona);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentComisionPersona.size());
    }
 
    @Command
    @NotifyChange({"comisionPersonaModel", "footer"})
    public void changeFilter() {
//    	currentComisionPersona = ComisionPersonaData.getFilterComisionPersonaes(comisionPersonaFilter);
    }
    
    @Command
    public void changeEditable(@BindingParam("comisiones") ComisionPersona com) {
    	HashedMap comision = new HashedMap();
    	comision.put("objeto", com);
    	Window window = (Window)Executions.createComponents(
				"../content/modalComisionPersona.zul", null, comision);
		window.doModal();
    	}
}