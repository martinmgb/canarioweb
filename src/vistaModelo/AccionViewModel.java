package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
 
import modelo.entidad.AccionData;
import modelo.entidad.Accion;
 
public class AccionViewModel {
 
    private static final String footerMessage = "En Total %d Acciones";
    private AccionFilter filtroAccion = new AccionFilter();
    List<Accion> accionesActuales = AccionData.getTodasAcciones();
 
    public AccionFilter getFiltroAccion() {
        return filtroAccion;
    }
 
    public ListModel<Accion> getAccionModel() {
        return new ListModelList<Accion>(accionesActuales);
    }
     
    public String getFooter() {
        return String.format(footerMessage, accionesActuales.size());
    }
 
    @Command
    @NotifyChange({"accionModel", "footer"})
    public void changeFilter() {
        accionesActuales = AccionData.getFiltroAcciones(filtroAccion);
    }
}
