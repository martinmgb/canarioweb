package vistaModelo;

public class PagoArrendamientoFilter {
	private String cedula="";
	private String nombre="";
	private String apellido="";
	private String fechapago="";
	private String montopagado="";
	private String descripcion="";


	public String getCedula() {
		return cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getFechapago() {
		return fechapago;
	}

	public String getMontopagado() {
		return montopagado;
	}

	public String getDescripcion() {
		return descripcion;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula==null?"":cedula.trim();
	}

	public void setNombre(String nombre) {
		this.nombre = nombre==null?"":nombre.trim();
	}

	public void setApellido(String apellido) {
		this.apellido = apellido==null?"":apellido.trim();
	}


}
