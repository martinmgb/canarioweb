package vistaModelo;

import java.util.List;

import modelo.entidad.Sancion;
import modelo.entidad.SancionData;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

public class SancionViewModel {
	
	 private static final String footerMessage = "Total de %d Sancion(es) ";
	    private SancionFilter sancionFilter = new SancionFilter();
	    List<Sancion> currentSancion = SancionData.getAllsanciones();
	 
	    public SancionFilter getsancionFilter() {
	        return sancionFilter;
	    }
	 
	    public ListModel<Sancion> getSancionModel() {
	        return new ListModelList<Sancion>(currentSancion);
	    }
	     
	    public String getFooter() {
	        return String.format(footerMessage, currentSancion.size());
	    }
	 
	    @Command
	    @NotifyChange({"sancionModel", "footer"})
	    public void changeFilter() {
	    	currentSancion = SancionData.getFiltersanciones(sancionFilter);
	    }		 
	   
}
