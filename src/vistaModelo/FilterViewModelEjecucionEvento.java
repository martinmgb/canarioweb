package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import modelo.entidad.EjecucionEvento;
import modelo.entidad.EjecucionEventoData;

 
public class FilterViewModelEjecucionEvento {
 
    private static final String footerMessage = "Total de %d Evento(s) Ejetutado(s) ";
    private EjecucionEventoFilter ejecucionEventoFilter = new EjecucionEventoFilter();
    List<EjecucionEvento> currentEjecucionEvento = EjecucionEventoData.getAllEjecucionEventos();
 
    public EjecucionEventoFilter getEjecucionEventoFilter() {
        return ejecucionEventoFilter;
    }
 
    public ListModel<EjecucionEvento> getEjecucionEventoModel() {
        return new ListModelList<EjecucionEvento>(currentEjecucionEvento);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentEjecucionEvento.size());
    }
 
    @Command
    @NotifyChange({"ejecucionEventoModel", "footer"})
    public void changeFilter() {
    	currentEjecucionEvento = EjecucionEventoData.getFilterEjecucionEventos(ejecucionEventoFilter);
    }
}