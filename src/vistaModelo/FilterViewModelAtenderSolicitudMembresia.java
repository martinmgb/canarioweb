package vistaModelo;

import java.util.List;

import org.apache.commons.collections.map.HashedMap;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import modelo.entidad.AtenderSolicitudMembresia;
import modelo.entidad.AtenderSolicitudMembresiaData;
import vistaModelo.AtenderSolicitudMembresiaFilter;

 
public class FilterViewModelAtenderSolicitudMembresia {
 
    private static final String footerMessage = "Total de %d Solicitudes(s) ";
    private AtenderSolicitudMembresiaFilter solicitudesMembresiaFilter = new AtenderSolicitudMembresiaFilter();
    List<AtenderSolicitudMembresia> currentSolicitudesMembresia = AtenderSolicitudMembresiaData.getAllSolicitudMembresias();
 
    public AtenderSolicitudMembresiaFilter getSolicitudesMembresiaFilter() {
        return solicitudesMembresiaFilter;
    }
 
    public ListModel<AtenderSolicitudMembresia> getSolicitudesMembresiaModel() {
        return new ListModelList<AtenderSolicitudMembresia>(currentSolicitudesMembresia);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentSolicitudesMembresia.size());
    }
 
    @Command
    @NotifyChange({"solicitudesMembresiaModel", "footer"})
    public void changeFilter() {
    	currentSolicitudesMembresia = AtenderSolicitudMembresiaData.getFilterSolicitudMembresias(solicitudesMembresiaFilter);
    }
    
    
    
    
    
    
    
    @Command
    public void changeEditable(@BindingParam("atendersolicitudes") AtenderSolicitudMembresia atenderSo) {
    	
    	HashedMap atenderSolicitud = new HashedMap();
    	
    	atenderSolicitud.put("objeto", atenderSo);
    	
    	
    	Window window = (Window)Executions.createComponents(
    			"../content/modalAprobarSolicitud.zul", null, atenderSolicitud);
		window.doModal();
    	}
}