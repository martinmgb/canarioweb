package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import modelo.entidad.Noticia;
import modelo.entidad.NoticiaData;

 
public class NoticiaViewModel {
 
    private static final String footerMessage = "Total de %d Noticia(s) ";
    private NoticiaFilter noticiaFilter = new NoticiaFilter();
    List<Noticia> currentNoticia = NoticiaData.getAllNoticias();
 
    public NoticiaFilter getNoticiaFilter() {
        return noticiaFilter;
    }
 
    public ListModel<Noticia> getNoticiaModel() {
        return new ListModelList<Noticia>(currentNoticia);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentNoticia.size());
    }
 
    @Command
    @NotifyChange({"noticiaModel", "footer"})
    public void changeFilter() {
    	currentNoticia = NoticiaData.getFilterNoticias(noticiaFilter);
    }
}