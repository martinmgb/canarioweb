package vistaModelo;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/
import java.util.List;

import org.apache.commons.collections.map.HashedMap;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Map;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import modelo.entidad.Comision;
import modelo.entidad.ComisionData;
import vistaModelo.ComisionFilter;

 
public class ComisionViewModel {
 
    private static final String footerMessage = "Total de %d Comision(s) ";
    private ComisionFilter comisionFilter = new ComisionFilter();
    List<Comision> currentComision = ComisionData.getAllComisiones();
 
    public ComisionFilter getComisionsFilter() {
        return comisionFilter;
    }
 
    public ListModel<Comision> getComisionModel() {
        return new ListModelList<Comision>(currentComision);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentComision.size());
    }
 
    @Command
    @NotifyChange({"comisionModel", "footer"})
    public void changeFilter() {
    	currentComision = ComisionData.getFilterComisiones(comisionFilter);
    }
    
  @Command
    public void changeEditable(@BindingParam("comisiones") Comision com) {
    	HashedMap comision = new HashedMap();
    	comision.put("objeto", com);
    	Window window = (Window)Executions.createComponents(
				"../content/modalComision.zul", null, comision);
		window.doModal();
    	}
}