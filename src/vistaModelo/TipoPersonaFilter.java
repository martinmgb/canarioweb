package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import modelo.entidad.TipoPersona;
import modelo.entidad.TipoPersonaData;

 
public class TipoPersonaFilter {
 
    private static final String footerMessage = "Total de %d Personas(s) ";
    private TipoPersonaViewModel personaFilter = new TipoPersonaViewModel();
    List<TipoPersona> currentPersona = TipoPersonaData.getAllPersonas();
 
    public TipoPersonaViewModel getPersonaFilter() {
        return personaFilter;
    }
 
    public ListModel<TipoPersona> getPersonaModel() {
        return new ListModelList<TipoPersona>(currentPersona);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentPersona.size());
    }
 
    @Command
    @NotifyChange({"personaModel", "footer"})
    public void changeFilter() {
    	currentPersona = TipoPersonaData.getFilterPersonas(personaFilter);
    }
}