package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import modelo.entidad.TipoIndicador;
import modelo.entidad.TipoIndicadorData;

 
public class TipoIndicadorViewModel {
 
    private static final String footerMessage = "Total de %d Tipo Indicador(s) ";
    private TipoIndicadorFilter tipoFilter = new TipoIndicadorFilter();
    List<TipoIndicador> currentTipo = TipoIndicadorData.getAllTipos();
 
    public TipoIndicadorFilter gettipoFilter() {
        return tipoFilter;
    }
 
    public ListModel<TipoIndicador> getTipoModel() {
        return new ListModelList<TipoIndicador>(currentTipo);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentTipo.size());
    }
 
    @Command
    @NotifyChange({"tipoModel", "footer"})
    public void changeFilter() {
    	currentTipo = TipoIndicadorData.getFilterTipos(tipoFilter);
    }
}