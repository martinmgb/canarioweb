package vistaModelo;
/*creado por Evelin Perez
fecha 14/06/2015*/

import java.util.List;

import org.apache.commons.collections.map.HashedMap;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import modelo.entidad.TipoComision;
import modelo.entidad.TipoNoticia;
import modelo.entidad.TipoNoticiaData;

 
public class TipoNoticiaViewModel {
 
    private static final String footerMessage = "Total de %d Tipo Noticia(s) ";
    private TipoNoticiaFilter tipoNoticiaFilter = new TipoNoticiaFilter();
    List<TipoNoticia> currentTipoNoticia = TipoNoticiaData.getAllTipoNoticia();
 
    public TipoNoticiaFilter getTipoNoticiasFilter() {
        return tipoNoticiaFilter;
    }
 
    public ListModel<TipoNoticia> getTipoNoticiaModel() {
        return new ListModelList<TipoNoticia>(currentTipoNoticia);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentTipoNoticia.size());
    }
 
    @Command
    @NotifyChange({"tipoNoticiaModel", "footer"})
    public void changeFilter() {
    	currentTipoNoticia = TipoNoticiaData.getFilterTipoNoticia(tipoNoticiaFilter);
    }
    
    @Command
    public void changeEditable(@BindingParam("tiponot") TipoNoticia tiponot) {
    	HashedMap obj = new HashedMap();
    	obj.put("objeto", tiponot);
    	Window window = (Window)Executions.createComponents(
				"../content/modalTipoNoticia.zul", null, obj);
		window.doModal();
    	}

}