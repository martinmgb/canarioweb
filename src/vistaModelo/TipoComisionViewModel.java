package vistaModelo;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/
import java.util.List;

import org.apache.commons.collections.map.HashedMap;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Map;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import modelo.entidad.TipoComision;
import modelo.entidad.TipoComisionData;
import vistaModelo.TipoComisionFilter;

 
public class TipoComisionViewModel {
 
    private static final String footerMessage = "Total de %d TipoComision(s) ";
    private TipoComisionFilter tipocomisionFilter = new TipoComisionFilter();
    List<TipoComision> currentTipoComision = TipoComisionData.getAllTipoComisiones();
 
    public TipoComisionFilter getTipoComisionsFilter() {
        return tipocomisionFilter;
    }
 
    public ListModel<TipoComision> getTipoComisionModel() {
        return new ListModelList<TipoComision>(currentTipoComision);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentTipoComision.size());
    }
 
    @Command
    @NotifyChange({"tipoComisionModel", "footer"})
    public void changeFilter() {
    	currentTipoComision = TipoComisionData.getFilterTipoComisiones(tipocomisionFilter);
    }
    
    @Command
    public void changeEditable(@BindingParam("tipocomisiones") TipoComision com) {
    	HashedMap comision = new HashedMap();
    	comision.put("objeto", com);
    	Window window = (Window)Executions.createComponents(
				"../content/modalTipoComision.zul", null, comision);
		window.doModal();
    	}
}