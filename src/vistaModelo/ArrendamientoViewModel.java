package vistaModelo;

import java.util.List;

import modelo.entidad.Arrendamiento;
import modelo.entidad.ArrendamientoData;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;

public class ArrendamientoViewModel {
	
	private static final String footerMessage = "Total de %d Arrendamiento(s) ";
    private ArrendamientoFilter arrendamientoFilter = new ArrendamientoFilter();
    List<Arrendamiento> currentArrendamiento = ArrendamientoData.getAllArrendamientos();
 
    public ArrendamientoFilter getArrendamientoFilter() {
        return arrendamientoFilter;
    }
 
    public ListModel<Arrendamiento> getArrendamientoModel() {
        return new ListModelList<Arrendamiento>(currentArrendamiento);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentArrendamiento.size());
    }
 
    @Command
    @NotifyChange({"arrendamientoModel", "footer"})
    public void changeFilter() {
    	currentArrendamiento = ArrendamientoData.getFilterArrendamientos(arrendamientoFilter);
    }

}
