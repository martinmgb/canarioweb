package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
 
import modelo.entidad.UnidadMedidaData;
import modelo.entidad.UnidadMedida;
 
public class UnidadMedidaFilterViewModel {
 
    private static final String footerMessage = "En Total %d Unidades de Medida";
    private UnidadMedidaFilter filtroUnidades = new UnidadMedidaFilter();
    List<UnidadMedida> unidadesActuales = UnidadMedidaData.getTodasUnidades();
 
    public UnidadMedidaFilter getFiltroUnidades() {
        return filtroUnidades;
    }
 
    public ListModel<UnidadMedida> getUnidadModel() {
        return new ListModelList<UnidadMedida>(unidadesActuales);
    }
     
    public String getFooter() {
        return String.format(footerMessage, unidadesActuales.size());
    }
 
    @Command
    @NotifyChange({"unidadModel", "footer"})
    public void changeFilter() {
        unidadesActuales = UnidadMedidaData.getFiltroUnidades(filtroUnidades);
    }
}
