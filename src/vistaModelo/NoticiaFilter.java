package vistaModelo;

public class NoticiaFilter {
	private String codigo="", tipo="",difusion="", imagen="",titulo="",descripcion="";

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo==null?"":codigo.trim();
	}

	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo==null?"":tipo.trim();
	}
	
	public String getDifusion() {
		return difusion;
	}

	public void setDifusion(String difusion) {
		this.difusion = difusion==null?"":difusion.trim();;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen==null?"":imagen.trim();
	}
		
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo==null?"":titulo.trim();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion==null?"":descripcion.trim();
	}
}
