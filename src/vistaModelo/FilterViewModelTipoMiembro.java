package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import modelo.entidad.TipoMiembro;
import modelo.entidad.TipoMiembroData;

 
public class FilterViewModelTipoMiembro {
 
    private static final String footerMessage = "Total de %d Miembro(s) ";
    private TipoMiembroFilter miembroFilter = new TipoMiembroFilter();
    List<TipoMiembro> currentMiembro = TipoMiembroData.getAllMiembros();
 
    public TipoMiembroFilter getMiembroFilter() {
        return miembroFilter;
    }
    
    
    
    public ListModel<TipoMiembro> getMiembroModel() {
        return new ListModelList<TipoMiembro>(currentMiembro);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentMiembro.size());
    }
 
    @Command
    @NotifyChange({"miembroModel", "footer"})
    public void changeFilter() {
    	currentMiembro = TipoMiembroData.getFilterMiembros(miembroFilter);
    }
}