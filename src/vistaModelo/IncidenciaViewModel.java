
package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import modelo.entidad.Incidencia;
import modelo.entidad.IncidenciaData;

 
public class IncidenciaViewModel {
 
    private static final String footerMessage = "Total de %d Incidencia(s) ";
    private IncidenciaFilter incidenciaFilter = new IncidenciaFilter();
    List<Incidencia> currentIncidencias = IncidenciaData.getAllIncidencias();
 
    public IncidenciaFilter getIncidenciaFilter() {
        return incidenciaFilter;
    }
 
    public ListModel<Incidencia> getIncidenciaModel() {
        return new ListModelList<Incidencia>(currentIncidencias);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentIncidencias.size());
    }
 
    @Command
    @NotifyChange({"incidenciaModel", "footer"})
    public void changeFilter() {
    	currentIncidencias = IncidenciaData.getFilterIncidencia(incidenciaFilter);
    }
}