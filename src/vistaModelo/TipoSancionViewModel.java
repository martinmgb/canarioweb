package vistaModelo;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import modelo.entidad.TipoSancion;
import modelo.entidad.TipoSancionData;

 
public class TipoSancionViewModel {
 
    private static final String footerMessage = "Total de %d Sanciones ";
    private TipoSancionFilter sancionFilter = new TipoSancionFilter();
    List<TipoSancion> currentSancion = TipoSancionData.getAllSanciones();
 
    public TipoSancionFilter getSancionFilter() {
        return sancionFilter;
    }
 
    public ListModel<TipoSancion> getSancionModel() {
        return new ListModelList<TipoSancion>(currentSancion);
    }
     
    public String getFooter() {
        return String.format(footerMessage, currentSancion.size());
    }
 
    @Command
    @NotifyChange({"sancionModel", "footer"})
    public void changeFilter() {
    	currentSancion = TipoSancionData.getFilterSanciones(sancionFilter);
    }
}