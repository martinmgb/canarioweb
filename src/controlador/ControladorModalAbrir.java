package controlador;

import modelo.entidad.Cargo;
import modelo.entidad.Comision;

import org.apache.commons.collections.map.HashedMap;

import java.awt.event.InputEvent;

import org.jfree.ui.Spinner;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;

import modelo.entidad.Comision;
import modelo.entidad.Indicador;
import modelo.entidad.TipoComision;
import modelo.entidad.Comision;
import modelo.entidad.TipoNoticia;

import org.apache.commons.collections.map.HashedMap;

import java.awt.event.InputEvent;

import org.jfree.ui.Spinner;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;

import modelo.entidad.Comision;

import org.apache.commons.collections.map.HashedMap;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;

public class ControladorModalAbrir extends SelectorComposer<Component> {
	private static final long serialVersionUID = 1L;

	@Listen("onClick = #orderBtn")
	public void showModal(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../modal_area.zul", null, null);
		window.doModal();


	}
	@Listen("onClick = #registrartipoeventos")
	public void showModalevento(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalRegistroTipoEvento.zul", null, null);
		window.doModal();
	}

	@Listen("onClick = #PagarsolicitudArren")
	public void showModalpagoarrem(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalRegistroPagoLuis.zul", null, null);
		window.doModal();


	}
	@Listen("onClick = #registrarUnidad")
	public void showModalUnidad(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalUnidadMedida.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #nuevaArea")
	public void show(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/registroArea.zul", null, null);
		window.doModal();
	}

	@Listen("onClick = #registrartdifusiones")
	public void showModalnoticia(Event e) {	
	//create a window programmatically and use it as a modal dialog.
			Window window = (Window)Executions.createComponents(
					"../content/modalTipoDifusion.zul", null, null);
			window.doModal();
}
	@Listen("onClick = #registrarpublico")
	public void showModalpublico(Event e) {	
	//create a window programmatically and use it as a modal dialog.
			Window window = (Window)Executions.createComponents(
					"../content/modalTipoPublico.zul", null, null);
			window.doModal();
}

	@Listen("onClick = #btnNuevaNoticia")
	public void showModalNoticia(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalNoticia.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #btnApelar")
	public void showModalApelar(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalApelar.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #btnSancionar")
	public void showModalSancionar(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalSancionar.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #personaBtn")
	public void showModalPersona(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalPersona.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #opinionesBtn")
	public void showModalOpinion(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalOpinion.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #registrarpagos")
	public void showModaregistrarpagos(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalRegistroPago.zul", null, null);
		window.doModal();
	}

	//visualiza el Detalle del Pago del Cliente que selecciona en la lista de Aprovado
	@Listen("onClick = #visualizarpagos")
	public void visualizarpagos() {
		Executions.sendRedirect("registroPagoCliente.zul");
	}

	@Listen("onClick = #registartipoindicador")
	public void showModalindicador(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalRegistroTipoIndicador.zul", null, null);
		window.doModal();
	}

	@Listen("onClick = 	#registarecurso")
	public void showModalrecurso(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalRegistroRecurso.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = 	#RegistrarCliente")
	public void showModalregistrocliente(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalRegistroCliente.zul", null, null);
		window.doModal();
	}


	@Listen("onClick = #registrarinsolventes")
	public void showModalinsolvente(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalRegistroInsolvente.zul", null, null);
		window.doModal();
	}  

	@Listen("onClick = #orderBtnIncidencia")
	public void showModalIncidecia(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalIncidencia.zul", null, null);
		window.doModal();


	}
	@Listen("onClick = #registrarComision")
	public void showModalComision(Event e) {
		Comision com = new Comision();
		HashedMap comision = new HashedMap();
		comision.put("objeto", com);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalComision.zul", null, comision);
		window.doModal();
	}
	@Listen("onClick = #registrarIndicador")
	public void showModalIndicador(Event e) {
		Indicador ind = new Indicador();
		HashedMap obj = new HashedMap();
		obj.put("objeto", ind);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalIndicador.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #registrartipoNoticia")
	public void showModalTipoNoticia(Event e) {
		TipoNoticia tiponot = new TipoNoticia();
		HashedMap obj = new HashedMap();
		obj.put("objeto", tiponot);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalTipoNoticia.zul", null, obj);
		window.doModal();
	}
	@Listen("onClick = #registrarTipoParentesco")
	public void showModalTipoParentesco(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalTipoParentesco.zul", null, null);
		window.doModal();
	}
	@Listen("onClick = #BtnRegistrarIncidencias")
	public void showModalRegistrarIncidencias(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/registrarIncidenciaEvento.zul", null, null);
		window.doModal();
	}
	@Listen("onSelect = #listaVisitas")
	    public void showVisitas(Event e) {
	    	Listbox list = new Listbox();
	    	list = (Listbox) e.getTarget();
	    	String id = list.getSelectedItem().getId();
	    	//alert(id);
	    	
	    	
	    	if(id.equals("listaVisitasBenAfil")){
	    		
	        	//create a window programmatically and use it as a modal dialog.
	        	Window window = (Window)Executions.createComponents(
	        			"../content/listaVisitas.zul", null, null);
	        	window.doModal();
	        	
	    	}else if(id.equals("listaVisitasBenAfil2")){
	    		//create a window programmatically and use it as a modal dialog.
	        	Window window = (Window)Executions.createComponents(
	        			"../content/listaVisitas.zul", null, null);
	        	window.doModal();
	    		
	    	}else if(id.equals("listaVisitasInvitados")){
	    		//create a window programmatically and use it as a modal dialog.
	        	Window window = (Window)Executions.createComponents(
	        			"../content/modal_anniadirVisitaInvitados.zul", null, null);
	        	window.doModal();
	    		
	    	}        	
	    }
	    
	    @Listen("onClick = #nuevaImagen")
	    public void showImagen(Event e) {
	        //create a window programmatically and use it as a modal dialog.
	        Window window = (Window)Executions.createComponents(
	        		"../content/modal_anniadirImagen.zul", null, null);
	        window.doModal();
	    }
	    
	    @Listen("onClick = #visitaInvitados")
	    public void showVisitaInvitados(Event e) {
	        //create a window programmatically and use it as a modal dialog.
	        Window window = (Window)Executions.createComponents(
	        		"../content/modal_anniadirVisitaInvitados.zul", null, null);
	        window.doModal();
	    }
	    
	    @Listen("onClick = #verificarEvento")
	    public void showVerificarEvento(Event e) {
	        //create a window programmatically and use it as a modal dialog.
	        Window window = (Window)Executions.createComponents(
	        		"../content/modalVerificarActividad.zul", null, null);
	        window.doModal();
	    }
	    
	    @Listen("onChange = #contRespuestas")
	    public int contarRespuestas(Event e) {

	    	Spinner spinner = new Spinner(0);
	    	int conttextbox = spinner.getValue();
	    	return conttextbox;

	    } 
	    
	    
	    @Listen("onClick = #registrarRespuesta")
	    public void showregistrarRespuesta(Event e) {
	        //create a window programmatically and use it as a modal dialog.
	        Window window = (Window)Executions.createComponents(
	        		"../content/modalRespuestas.zul", null, null);
	        int nrotextbox = contarRespuestas(e);
	        String numtextbox = Integer.toString(nrotextbox);
	        alert(numtextbox);
	        window.doModal();
	    }
	    
	@Listen("onClick = #registrarCargo")
	public void showModalCargo(Event e) {
		Cargo car = new Cargo();
		HashedMap cargo = new HashedMap();
		cargo.put("objeto", car);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalCargo.zul", null, cargo);
		window.doModal();
	}
	@Listen("onClick = #registrarTipoComision")
	public void showModalTipoComision(Event e) {
		TipoComision com = new TipoComision();
		HashedMap tipocomision = new HashedMap();
		tipocomision.put("objeto", com);
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalTipoComision.zul", null, tipocomision);
		window.doModal();
	}
}