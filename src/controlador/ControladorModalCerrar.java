package controlador;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

public class ControladorModalCerrar extends SelectorComposer<Component> {
	private static final long serialVersionUID = 1L;

	@Wire
	Window modalDialog;

	@Listen("onClick = #closeBtn")
	public void showModal(Event e) {
		modalDialog.detach();
	}

	@Listen("onClick = #closeBtneventos")
	public void showModaleventos(Event e) {
		modalDialog.detach();
	}

	@Listen("onClick = #closeBtn1")
	public void showModaleventos1(Event e) {
		modalDialog.detach();
	}
	@Listen("onClick = #closeUnidad")
	public void showModalUnidad(Event e) {
		modalDialog.detach();
	}
	@Listen("onClick = #closeArea")
	public void showModalArea(Event e) {
		modalDialog.detach();
	}
	@Listen("onClick = #closeBtnApelar")
	public void showModalApelar(Event e) {
		modalDialog.detach();
	}
	@Listen("onClick = #closeBtninsolventes")
	public void showModalinsolventes(Event e) {
		modalDialog.detach();
	}
	
//	@Listen("onClick = #closeBtnEncuesta")
//	public void showModalinsolventes(Event e) {
//		modalDialog.detach();
//	}
	
}