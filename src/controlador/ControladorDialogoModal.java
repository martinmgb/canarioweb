package controlador;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zul.Window;

public class ControladorDialogoModal extends SelectorComposer<Component> {
	private static final long serialVersionUID = 1L;

	@Listen("onClick = #orderBtn")
	public void showModalArea(Event e) {
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalArea.zul", null, null);
		window.doModal();


	}
	@Listen("onClick = #registrarEventos")
	public void showModalEvento(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalEvento.zul", null, null);
		window.doModal();
	}
	
	@Listen("onClick = #registrarActividades")
	public void showModalActividad(Event e) {	
		//create a window programmatically and use it as a modal dialog.
		Window window = (Window)Executions.createComponents(
				"../content/modalActividad.zul", null, null);
		window.doModal();
	}
	//HECHO POR LYNNN
	@Listen("onClick = #registrarcargos")
	public void showModalcargo(Event e) {	
	//create a window programmatically and use it as a modal dialog.
			Window window = (Window)Executions.createComponents(
					"../content/modalTipoCargo.zul", null, null);
			window.doModal();
	}
	
	@Listen("onClick = #registrarmiembro")
	public void showModaltipomiembro(Event e) {	
	//create a window programmatically and use it as a modal dialog.
			Window window = (Window)Executions.createComponents(
					"../content/modalTipoMiembro.zul", null, null);
			window.doModal();
	}

	@Listen("onClick = #registrarSancion")
	public void showModalsancion(Event e) {	
	//create a window programmatically and use it as a modal dialog.
			Window window = (Window)Executions.createComponents(
					"modalTipoSancion.zul", null, null);
			window.doModal();
}
	
	
	@Listen("onClick = #verAprobacion")
	public void showModalAprobar(Event e) {	
	//create a window programmatically and use it as a modal dialog.
			Window window = (Window)Executions.createComponents(
					"../content/modalAprobarSolicitud.zul", null, null);
			window.doModal();
}
	
	
	
	
}