package modelo.entidad;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.TipoOpinionViewModel;



public class AprobarPagoArrendamientoData {

	private static List<AprobarPagoArrendamiento> aprobarpago = new ArrayList<AprobarPagoArrendamiento>();
	static {
		aprobarpago.add(new AprobarPagoArrendamiento("01/05/2015", "Tamarindo","5000", "7000"));
		aprobarpago.add(new AprobarPagoArrendamiento("15/05/2015","Tamarindo", "7000", "0"));
	
	}

	public static List<AprobarPagoArrendamiento> getAllOpiniones() {
		return new ArrayList<AprobarPagoArrendamiento>(aprobarpago);
	}
	public static AprobarPagoArrendamiento[] getAllOpinionesArray() {
		return aprobarpago.toArray(new AprobarPagoArrendamiento[aprobarpago.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<AprobarPagoArrendamiento> getFilterOpiniones(TipoOpinionViewModel opinionFilter) {
		List<AprobarPagoArrendamiento> someopiniones = new ArrayList<AprobarPagoArrendamiento>();
		String cod = opinionFilter.getCodigo().toLowerCase();
		String tip = opinionFilter.getTipo().toLowerCase();
		String des = opinionFilter.getDescripcion().toLowerCase();

		for (Iterator<AprobarPagoArrendamiento> i = aprobarpago.iterator(); i.hasNext();) {
			AprobarPagoArrendamiento tmp = i.next();
			if (tmp.getFechapago().toLowerCase().contains(cod) &&
					tmp.getMontopago().toLowerCase().contains(tip)  &&
					tmp.getResto().toLowerCase().contains(des)) {
				someopiniones.add(tmp);
			}
		}
		return someopiniones;
	}

	// This Method only used in "Header and footer" Demo
	public static List<AprobarPagoArrendamiento> getOpinionesByCodigo(String codigo) {
		List<AprobarPagoArrendamiento> someopiniones = new ArrayList<AprobarPagoArrendamiento>();
		for (Iterator<AprobarPagoArrendamiento> i = aprobarpago.iterator(); i.hasNext();) {
			AprobarPagoArrendamiento tmp = i.next();
			if (tmp.getFechapago().equalsIgnoreCase(codigo)){
				someopiniones.add(tmp);
			}
		}
		return someopiniones;
	}
}