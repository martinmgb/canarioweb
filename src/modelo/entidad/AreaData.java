package modelo.entidad;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
 
import modelo.entidad.Area;
import vistaModelo.AreaFilter;
 
public class AreaData {
 
    private static List<Area> areas = new ArrayList<Area>();
    static {
        areas.add(new Area(1, "Sala de Fiestas", "Social", 115, 1200, "No Disponible","imagen"));
        areas.add(new Area(2, "Sala de Fiestas", "Social", 115, 1200, "Disponible","imagen"));
        areas.add(new Area(3, "Sala de Fiestas", "Social", 115, 1200, "Disponible","imagen"));
        areas.add(new Area(4, "Sala de Fiestas", "Social", 115, 1200, "Disponible","imagen"));
        areas.add(new Area(5, "Sala de Fiestas", "Social", 115, 1200, "Disponible","imagen"));
        areas.add(new Area(6, "Sala de Fiestas", "Social", 115, 1200, "Disponible","imagen"));
        areas.add(new Area(7, "Sala de Fiestas", "Social", 115, 1200, "Disponible","imagen"));
        areas.add(new Area(8, "Sala de Fiestas", "Social", 115, 1200, "No Disponible","imagen"));
        areas.add(new Area(9, "Sala de Fiestas", "Social", 115, 1200, "Disponible","imagen"));
        areas.add(new Area(10, "Sala de Fiestas", "Social", 115, 1200, "Disponible","imagen"));
        areas.add(new Area(11, "Sala de Fiestas", "Social", 115, 1200, "Disponible","imagen"));
        areas.add(new Area(12, "Sala de Fiestas", "Social", 115, 1200, "Disponible","imagen"));
        areas.add(new Area(13, "Piscina", "Social", 115, 1200, "Disponible","imagen"));
        areas.add(new Area(14, "Sala de Fiestas", "Social", 115, 1200, "Disponible","imagen"));
        areas.add(new Area(15, "Sala de Fiestas", "Social", 115, 1200, "Disponible","imagen"));
        areas.add(new Area(16, "Sala de Fiestas", "Social", 115, 1200, "No Disponible","imagen"));
        areas.add(new Area(17, "Sala de Fiestas", "Social", 115, 1200, "No Disponible","imagen"));
        areas.add(new Area(18, "Sala de Fiestas", "Social", 115, 1200, "No Disponible","imagen"));
        areas.add(new Area(19, "Sala de Fiestas", "Social", 115, 1200, "No Disponible","imagen"));
    }
 
    public static List<Area> getTodasAreas() {
        return new ArrayList<Area>(areas);
    }
    public static Area[] getAllFoodsArray() {
        return areas.toArray(new Area[areas.size()]);
    }
 
    // This Method only used in "Data Filter" Demo
    public static List<Area> getFiltroAreas(AreaFilter foodFilter) {
        List<Area> someAreas = new ArrayList<Area>();
        String nombre = foodFilter.getNombre().toLowerCase();
        String codigo = foodFilter.getCodigo().toLowerCase();
        String capacidad = foodFilter.getCapasidad().toLowerCase();
        String precio = foodFilter.getPrecio().toLowerCase();
        String condicion = foodFilter.getCondicion().toLowerCase();
        String tipo = foodFilter.getTipo().toLowerCase();
        
        for (Iterator<Area> i = areas.iterator(); i.hasNext();) {
            Area tmp = i.next();
            if (tmp.getNombre().toLowerCase().contains(nombre) 
            		&& tmp.getCondicion().toLowerCase().contains(condicion)
            		&& tmp.getTipo().toLowerCase().contains(tipo)
            		) {
                someAreas.add(tmp);
            }
        }
        return someAreas;
    }
 
    // This Method only used in "Header and footer" Demo
    public static List<Area> getAreasNombre(String nombre) {
        List<Area> someAreas = new ArrayList<Area>();
        for (Iterator<Area> i = areas.iterator(); i.hasNext();) {
            Area tmp = i.next();
            if (tmp.getNombre().equalsIgnoreCase(nombre)){
                someAreas.add(tmp);
            }
        }
        return someAreas;
    }
    
    public static List<Area> getAreasTipo(String nombre) {
        List<Area> someAreas = new ArrayList<Area>();
        for (Iterator<Area> i = areas.iterator(); i.hasNext();) {
            Area tmp = i.next();
            if (tmp.getTipo().equalsIgnoreCase(nombre)){
                someAreas.add(tmp);
            }
        }
        return someAreas;
    }
    
    public static List<Area> getAreasCondicion(String nombre) {
        List<Area> someAreas = new ArrayList<Area>();
        for (Iterator<Area> i = areas.iterator(); i.hasNext();) {
            Area tmp = i.next();
            if (tmp.getCondicion().equalsIgnoreCase(nombre)){
                someAreas.add(tmp);
            }
        }
        return someAreas;
    }
}
