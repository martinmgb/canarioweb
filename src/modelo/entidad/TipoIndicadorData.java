package modelo.entidad;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import vistaModelo.TipoIndicadorFilter;
import modelo.entidad.TipoIndicador;

public class TipoIndicadorData {

	private static List<TipoIndicador> tipos = new ArrayList<TipoIndicador>();
	static {
		tipos.add(new TipoIndicador("001", "Evento", "XXXX"));
		tipos.add(new TipoIndicador("002", "Arrendamiento", "XXXX"));
		tipos.add(new TipoIndicador("003", "Miembro", "XXXX"));


	}

	public static List<TipoIndicador> getAllTipos() {
		return new ArrayList<TipoIndicador>(tipos);
	}
	public static TipoIndicador[] getAllTiposArray() {
		return tipos.toArray(new TipoIndicador[tipos.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<TipoIndicador> getFilterTipos(TipoIndicadorFilter tiposFilter) {
		List<TipoIndicador> sometipos = new ArrayList<TipoIndicador>();
		String cod = tiposFilter.getCodigo().toLowerCase();
		String nom = tiposFilter.getNombre().toLowerCase();
		String des = tiposFilter.getDescripcion().toLowerCase();

		for (Iterator<TipoIndicador> i = tipos.iterator(); i.hasNext();) {
			TipoIndicador tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getNombre().toLowerCase().contains(nom)  &&
					tmp.getDescripcion().toLowerCase().contains(des)) {
				sometipos.add(tmp);
			}
		}
		return sometipos;
	}

	// This Method only used in "Header and footer" Demo
	public static List<TipoIndicador> getRecursosByCodigo(String codigo) {
		List<TipoIndicador> sometipos = new ArrayList<TipoIndicador>();
		for (Iterator<TipoIndicador> i = tipos.iterator(); i.hasNext();) {
			TipoIndicador tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				sometipos.add(tmp);
			}
		}
		return sometipos;
	}
}