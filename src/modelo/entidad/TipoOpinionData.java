package modelo.entidad;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.TipoOpinionViewModel;



public class TipoOpinionData {

	private static List<TipoOpinion> opiniones = new ArrayList<TipoOpinion>();
	static {
		opiniones.add(new TipoOpinion("001", "Opinion 1", "XXXX"));
		opiniones.add(new TipoOpinion("002", "Opinion 2", "XXXX"));
		opiniones.add(new TipoOpinion("003", "Opinion 3", "XXXX"));
		opiniones.add(new TipoOpinion("004", "Opinion 4", "XXXX"));
	}

	public static List<TipoOpinion> getAllOpiniones() {
		return new ArrayList<TipoOpinion>(opiniones);
	}
	public static TipoOpinion[] getAllOpinionesArray() {
		return opiniones.toArray(new TipoOpinion[opiniones.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<TipoOpinion> getFilterOpiniones(TipoOpinionViewModel opinionFilter) {
		List<TipoOpinion> someopiniones = new ArrayList<TipoOpinion>();
		String cod = opinionFilter.getCodigo().toLowerCase();
		String tip = opinionFilter.getTipo().toLowerCase();
		String des = opinionFilter.getDescripcion().toLowerCase();

		for (Iterator<TipoOpinion> i = opiniones.iterator(); i.hasNext();) {
			TipoOpinion tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getTipo().toLowerCase().contains(tip)  &&
					tmp.getDescripcion().toLowerCase().contains(des)) {
				someopiniones.add(tmp);
			}
		}
		return someopiniones;
	}

	// This Method only used in "Header and footer" Demo
	public static List<TipoOpinion> getOpinionesByCodigo(String codigo) {
		List<TipoOpinion> someopiniones = new ArrayList<TipoOpinion>();
		for (Iterator<TipoOpinion> i = opiniones.iterator(); i.hasNext();) {
			TipoOpinion tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				someopiniones.add(tmp);
			}
		}
		return someopiniones;
	}
}