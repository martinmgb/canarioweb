package modelo.entidad;

public class TipoIncidencia {
	private String codigo;
	private String tipo;
	private String descripcion;

	public TipoIncidencia(String codigo, String tipo, String descripcion) {
		// TODO Auto-generated constructor stub
		this.codigo = codigo;
		this.tipo = tipo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	

}
