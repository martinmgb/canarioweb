package modelo.entidad;

public class Actividad {
	private String codigo;
	private String descripcion;
	

	public Actividad(String codigo, String descripcion) {
		// TODO Auto-generated constructor stub
		this.codigo = codigo;
		this.descripcion = descripcion;
		
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	

}
