package modelo.entidad;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.TipoEventoFilter;
import vistaModelo.TipoIndicadorFilter;

public class TipoEventoData {

	private static List<TipoEvento> tipoevento = new ArrayList<TipoEvento>();
	static {
		tipoevento.add(new TipoEvento("001", "Deportivo", "XXXX"));
		tipoevento.add(new TipoEvento("002", "Cultural", "XXXX"));
		tipoevento.add(new TipoEvento("003", "Religioso", "XXXX"));


	}

	public static List<TipoEvento> getAllTipos() {
		return new ArrayList<TipoEvento>(tipoevento);
	}
	

	// This Method only used in "Data Filter" Demo
	public static List<TipoEvento> getFilterTipos(TipoEventoFilter tiposFilter) {
		List<TipoEvento> sometiposevento = new ArrayList<TipoEvento>();
		String cod = tiposFilter.getCodigo().toLowerCase();
		String nom = tiposFilter.getNombre().toLowerCase();

		for (Iterator<TipoEvento> i = tipoevento.iterator(); i.hasNext();) {
			TipoEvento tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getNombre().toLowerCase().contains(nom)) {
				sometiposevento.add(tmp);
			}
		}
		return sometiposevento;
	}

	// This Method only used in "Header and footer" Demo
	public static List<TipoEvento> getRecursosByCodigo(String codigo) {
		List<TipoEvento> sometipos = new ArrayList<TipoEvento>();
		for (Iterator<TipoEvento> i = tipoevento.iterator(); i.hasNext();) {
			TipoEvento tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				sometipos.add(tmp);
			}
		}
		return sometipos;
	}
	
}
