package modelo.entidad;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.ClienteFilter;
import vistaModelo.InsolventeFilter;

public class ClienteData {
	private static List<Cliente> clientes = new ArrayList<Cliente>();
	static {
		clientes.add(new Cliente("21468751","Guillermo","guillermo123@gmail.com","04245368566","c","c","c","c","c"));
		clientes.add(new Cliente("201467752","Mariel","marielvera21@gmail.com","04245368566","c","c","c","c","c"));
		clientes.add(new Cliente("221478752","Aura","aura232@gmail.com","04245368566","C","c","c","c","c"));
		
	}
	//Retorna todos los Insolventes
	public static List<Cliente> getTodosClientes() {
		return new ArrayList<Cliente>(clientes);
	}
	
	
    // Metodo que obtiene por filtro 
    public static List<Cliente> getFiltroCliente(ClienteFilter foodFilter) {
        List<Cliente> someinsolvente = new ArrayList<Cliente>();
        String cedula = foodFilter.getCedula().toLowerCase();
        String nombre = foodFilter.getNombre().toLowerCase();
        for (Iterator<Cliente> i = clientes.iterator(); i.hasNext();) {
        	Cliente tmp = i.next();
            if (tmp.getCedula().toLowerCase().contains(cedula) 
            		&& tmp.getNombre().toLowerCase().contains(nombre)
            		&& tmp.getNombre().toLowerCase().contains(nombre)
            		) {
            	someinsolvente.add(tmp);
            }
        }
        return someinsolvente;
    }
    
}
