package modelo.entidad;

public class TipoIndicador {
	private String codigo;
	private String nombre;
	private String descripcion;

	public TipoIndicador(String codigo, String nombre, String descripcion) {
		// TODO Auto-generated constructor stub
		this.codigo = codigo;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	

}