

package modelo.entidad;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import vistaModelo.IncidenciaFilter;
import modelo.entidad.Incidencia;

public class IncidenciaData {

	private static List<Incidencia> incidencias = new ArrayList<Incidencia>();
	static {
		incidencias.add(new Incidencia("001", "7254514", "Pedro", "Peña", "03/02/2015", "Evento", "E01", "XXXXXXXX" ));
	}

	public static List<Incidencia> getAllIncidencias() {
		return new ArrayList<Incidencia>(incidencias);
	}
	public static Incidencia[] getAllAreasArray() {
		return incidencias.toArray(new Incidencia[incidencias.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<Incidencia> getFilterIncidencia(IncidenciaFilter incidenciasFilter) {
		List<Incidencia> someincidencias = new ArrayList<Incidencia>();
		String cod = incidenciasFilter.getCodigo().toLowerCase();
		String ced = incidenciasFilter.getCedula().toLowerCase();
		String nom = incidenciasFilter.getNombre().toLowerCase();
		String ape = incidenciasFilter.getApellido().toLowerCase();
		String fec = incidenciasFilter.getFecha().toLowerCase();
		String tip = incidenciasFilter.getTipo().toLowerCase();
		String cca = incidenciasFilter.getCodigocaso().toLowerCase();
		String des = incidenciasFilter.getDescripcion().toLowerCase();

		for (Iterator<Incidencia> i = incidencias.iterator(); i.hasNext();) {
			Incidencia tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getCedula().toLowerCase().contains(ced) &&
					tmp.getNombre().toLowerCase().contains(nom) &&
					tmp.getApellido().toLowerCase().contains(ape) &&
					tmp.getFecha().toString().toLowerCase().contains(fec) &&
					tmp.getTipo().toLowerCase().contains(tip)  &&
					tmp.getCodigocaso().toLowerCase().contains(cca)  &&
					tmp.getDescripcion().toLowerCase().contains(des)
				) {
				someincidencias.add(tmp);
			}
		}
		return someincidencias;
	}

	// This Method only used in "Header and footer" Demo
	public static List<Incidencia> getIncidenciasByCodigo(String codigo) {
		List<Incidencia> someincidencias = new ArrayList<Incidencia>();
		for (Iterator<Incidencia> i = incidencias.iterator(); i.hasNext();) {
			Incidencia tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				someincidencias.add(tmp);
			}
		}
		return someincidencias;
	}
}