package modelo.entidad;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.SolicitudEventoFilter;
import vistaModelo.SolicitudEventoViewModel;
import modelo.entidad.SolicitudEvento;

public class SolicitudEventoData {

	private static List<SolicitudEvento> solicitudEventos = new ArrayList<SolicitudEvento>();
	static {
		solicitudEventos.add(new SolicitudEvento("001", "Tamarindo", "XXXX", "Tamarindo"));
		solicitudEventos.add(new SolicitudEvento("002", "Sala de Fiestas", "XXXX", "Tamarindo"));
		solicitudEventos.add(new SolicitudEvento("003", "Cancha de Bolas Criollas", "XXXX", "Tamarindo"));
		solicitudEventos.add(new SolicitudEvento("004", "Tamarindo", "XXXX", "Tamarindo"));
		solicitudEventos.add(new SolicitudEvento("005", "Piscina", "XXXX", "Tamarindo"));
		solicitudEventos.add(new SolicitudEvento("006", "Tamarindo", "XXXX", "Tamarindo"));
		solicitudEventos.add(new SolicitudEvento("007", "Piscina", "XXXX", "Tamarindo"));
	}

	public static List<SolicitudEvento> getAllSolicitudEventos() {
		return new ArrayList<SolicitudEvento>(solicitudEventos);
	}
	public static SolicitudEvento[] getAllSolicitudEventosArray() {
		return solicitudEventos.toArray(new SolicitudEvento[solicitudEventos.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<SolicitudEvento> getFilterSolicitudEventos(SolicitudEventoFilter filtroSolicitudEvento) {
		List<SolicitudEvento> somesolicitudEventos = new ArrayList<SolicitudEvento>();
		String cod = filtroSolicitudEvento.getCodigo().toLowerCase();
		String nom = filtroSolicitudEvento.getNombre().toLowerCase();

		for (Iterator<SolicitudEvento> i = solicitudEventos.iterator(); i.hasNext();) {
			SolicitudEvento tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getNombre().toLowerCase().contains(nom)) {
				somesolicitudEventos.add(tmp);
			}
		}
		return somesolicitudEventos;
	}

	// This Method only used in "Header and footer" Demo
	public static List<SolicitudEvento> getSolicitudEventosByCodigo(String codigo) {
		List<SolicitudEvento> somesolicitudEventos = new ArrayList<SolicitudEvento>();
		for (Iterator<SolicitudEvento> i = solicitudEventos.iterator(); i.hasNext();) {
			SolicitudEvento tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				somesolicitudEventos.add(tmp);
			}
		}
		return somesolicitudEventos;
	}
}