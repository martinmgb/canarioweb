package modelo.entidad;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import vistaModelo.SancionFilter;

public class SancionData {
	private static List<Sancion> sanciones = new ArrayList<Sancion>();
	static {
		sanciones.add(new Sancion("001", "Grave", "XXXX","23/02/2015","16278545","Maria Perez","I001" ));
	}

	public static List<Sancion> getAllsanciones() {
		return new ArrayList<Sancion>(sanciones);
	}
	public static Sancion[] getAllsancionesArray() {
		return sanciones.toArray(new Sancion[sanciones.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<Sancion> getFiltersanciones(SancionFilter sancionFilter) {
		List<Sancion> somesanciones = new ArrayList<Sancion>();
		String cod = sancionFilter.getCodigo().toLowerCase();
		String tip = sancionFilter.getTipo().toLowerCase();
		String fech = sancionFilter.getFecha().toLowerCase();
		String ced = sancionFilter.getCedula().toLowerCase();
		String nom = sancionFilter.getNombre().toLowerCase();
		String codI = sancionFilter.getCodIncidencia().toLowerCase();

		for (Iterator<Sancion> i = sanciones.iterator(); i.hasNext();) {
			Sancion tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getTipo().toLowerCase().contains(tip) &&
					tmp.getFecha().toString().contains(fech) &&
					tmp.getCedula().toLowerCase().contains(ced) &&
					tmp.getNombre().toLowerCase().contains(nom) &&
					tmp.getCodIncidencia().toLowerCase().contains(codI)) {
				somesanciones.add(tmp);
			}
		}
		return somesanciones;
	}

	// This Method only used in "Header and footer" Demo
	public static List<Sancion> getSancionesByCodigo(String codigo) {
		List<Sancion> somesanciones = new ArrayList<Sancion>();
		for (Iterator<Sancion> i = sanciones.iterator(); i.hasNext();) {
			Sancion tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				somesanciones.add(tmp);
			}
		}
		return somesanciones;
	}
}
