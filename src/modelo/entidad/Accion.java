package modelo.entidad;

import java.io.Serializable;
import java.util.Date;

public class Accion implements  Serializable{
	  
	
	private static final long serialVersionUID = 1L;
	private boolean editingStatus=false;
	private char estatus= 'A';
	

	private int id;
	private String miembro;
	private Date tiempo;
	private String disponibilidad;
	
    public Accion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Accion(int id, String miembro, Date tiempo, String disponibilidad) {
		super();
		this.id = id;
		this.miembro = miembro;
		this.tiempo = tiempo;
		this.disponibilidad = disponibilidad;
	}

	public boolean isEditingStatus() {
		return editingStatus;
	}

	public void setEditingStatus(boolean editingStatus) {
		this.editingStatus = editingStatus;
	}

	public char getEstatus() {
		return estatus;
	}

	public void setEstatus(char estatus) {
		this.estatus = estatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMiembro() {
		return miembro;
	}

	public void setMiembro(String miembro) {
		this.miembro = miembro;
	}

	public Date getTiempo() {
		return tiempo;
	}

	public void setTiempo(Date tiempo) {
		this.tiempo = tiempo;
	}

	public String getDisponibilidad() {
		return disponibilidad;
	}

	public void setDisponibilidad(String disponibilidad) {
		this.disponibilidad = disponibilidad;
	}
    
    

	
     
     
	
}
