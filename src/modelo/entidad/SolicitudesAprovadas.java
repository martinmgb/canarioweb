package modelo.entidad;

import java.io.Serializable;

public class SolicitudesAprovadas implements  Serializable {
	private static final long serialVersionUID = 1L;
	private String codigo;
	private String cedula;
	private String nombre;
	private String apellido;
	private String tipo;
	private String area;
	private String fecha;
	private String monto;
	private String montopagado;



	public SolicitudesAprovadas(String codigo,String cedula, String nombre,String apellido,String tipo, String area,
			String fecha, String monto,String montopagado ) {
		super();
		this.codigo = codigo;
		this.cedula= cedula;
		this.nombre = nombre;
		this.apellido=apellido;
		this.tipo = tipo;
		this.area = area;
		this.fecha = fecha;
		this.monto = monto;
		this.montopagado=montopagado; 
	}

	public SolicitudesAprovadas() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCodigo() {
		return codigo;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getMontopagado() {
		return montopagado;
	}
	public void setMontopagado(String montopagado) {
		this.montopagado = montopagado;
	}

}
