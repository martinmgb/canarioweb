package modelo.entidad;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import vistaModelo.ApelacionFilter;
import modelo.entidad.Apelacion;

public class ApelacionData {

	private static List<Apelacion> apelaciones = new ArrayList<Apelacion>();
	static Sancion sancion = new Sancion("1","tipo","asd","15/05/2015","23542438", "Martín Gutiérrez", "Incidencia");
	static {
		apelaciones.add(new Apelacion("001", "Martin Gutierrez", "Apelacion 1",new SimpleDateFormat("dd-MM-yyyy").format(new Date()) ));
		apelaciones.add(new Apelacion("002", "Martin Gutierrez", "Apelacion 2", new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		apelaciones.add(new Apelacion("003", "Martin Gutierrez", "Apelacion 3", new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		apelaciones.add(new Apelacion("004",  "Martin Gutierrez","Apelacion 4", new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		apelaciones.add(new Apelacion("005", "Martin Gutierrez", "Apelacion 5", new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		apelaciones.add(new Apelacion("006",  "Martin Gutierrez","Apelacion 6", new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		apelaciones.add(new Apelacion("007",  "Martin Gutierrez","Apelacion 7", new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		apelaciones.add(new Apelacion("008",  "Martin Gutierrez","Apelacion 8", new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		apelaciones.add(new Apelacion("009",  "Martin Gutierrez","Apelacion 9",new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		apelaciones.add(new Apelacion("010", "Martin Gutierrez", "Apelacion 10",new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		apelaciones.add(new Apelacion("012", "Martin Gutierrez", "Apelacion 11", new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		apelaciones.add(new Apelacion("013", "Martin Gutierrez", "Apelacion 12", new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		apelaciones.add(new Apelacion("014", "Martin Gutierrez", "Apelacion 13",new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		apelaciones.add(new Apelacion("015", "Martin Gutierrez", "Apelacion 14",new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		apelaciones.add(new Apelacion("016", "Martin Gutierrez", "Apelacion 15", new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		apelaciones.add(new Apelacion("017",  "Martin Gutierrez","Apelacion 16", new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
		
	}

	public static List<Apelacion> getAllApelaciones() {
		return new ArrayList<Apelacion>(apelaciones);
	}
	public static Apelacion[] getAllApelacionesArray() {
		return apelaciones.toArray(new Apelacion[apelaciones.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<Apelacion> getFilterApelaciones(ApelacionFilter apelacionFilter) {
		List<Apelacion> someapelaciones = new ArrayList<Apelacion>();
		String cod = apelacionFilter.getCodigo().toLowerCase();
		String nom = apelacionFilter.getMiembro().toLowerCase();
		String des = apelacionFilter.getDescripcion().toLowerCase();

		for (Iterator<Apelacion> i = apelaciones.iterator(); i.hasNext();) {
			Apelacion tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getPersona().toLowerCase().contains(nom) &&
					tmp.getDescripcion().toLowerCase().contains(des)) {
				someapelaciones.add(tmp);
			}
		}
		return someapelaciones;
	}

	// This Method only used in "Header and footer" Demo
	public static List<Apelacion> getComisionesByCodigo(String codigo) {
		List<Apelacion> someapelaciones = new ArrayList<Apelacion>();
		for (Iterator<Apelacion> i = apelaciones.iterator(); i.hasNext();) {
			Apelacion tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				someapelaciones.add(tmp);
			}
		}
		return someapelaciones;
	}
}