package modelo.entidad;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import vistaModelo.TipoCargoFilter;
import modelo.entidad.TipoCargo;

public class TipoCargoData {

	private static List<TipoCargo> cargos = new ArrayList<TipoCargo>();
	static {
		cargos.add(new TipoCargo("001", "Junta Directiva"));
		cargos.add(new TipoCargo("002", "Secretaria"));
		cargos.add(new TipoCargo("003", "Mantenimiento"));
	}

	public static List<TipoCargo> getAllCargos() {
		return new ArrayList<TipoCargo>(cargos);
	}
	public static TipoCargo[] getAllCargoArray() {
		return cargos.toArray(new TipoCargo[cargos.size()]);
	}

		// This Method only used in "Data Filter" Demo
		public static List<TipoCargo> getFilterCargos(TipoCargoFilter cargoFilter) {
			List<TipoCargo> somecargos = new ArrayList<TipoCargo>();
			String cod = cargoFilter.getCodigo().toLowerCase();
			String tip = cargoFilter.getTipo().toLowerCase();
		
			for (Iterator<TipoCargo> i = cargos.iterator(); i.hasNext();) {
				TipoCargo tmp = i.next();
				if (tmp.getCodigo().toLowerCase().contains(cod) &&
						tmp.getTipo().toLowerCase().contains(tip)  ) {
					somecargos.add(tmp);
				}
			}
			return somecargos;
		}

		// This Method only used in "Header and footer" Demo
		public static List<TipoCargo> getCargosByCodigo(String codigo) {
			List<TipoCargo> somecargos = new ArrayList<TipoCargo>();
			for (Iterator<TipoCargo> i = cargos.iterator(); i.hasNext();) {
				TipoCargo tmp = i.next();
				if (tmp.getCodigo().equalsIgnoreCase(codigo)){
					somecargos.add(tmp);
				}
			}
			return somecargos;
		}
}