package modelo.entidad;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import vistaModelo.AtenderSolicitudMembresiaFilter;
import modelo.entidad.AtenderSolicitudMembresia;

public class AtenderSolicitudMembresiaData {

	private static List<AtenderSolicitudMembresia> solicitudesMembresia = new ArrayList<AtenderSolicitudMembresia>();
	static {
		solicitudesMembresia.add(new AtenderSolicitudMembresia("001", "Mario"));
		solicitudesMembresia.add(new AtenderSolicitudMembresia("002", "Petra"));
		solicitudesMembresia.add(new AtenderSolicitudMembresia("003", "Stevan"));
	}

	public static List<AtenderSolicitudMembresia> getAllSolicitudMembresias() {
		return new ArrayList<AtenderSolicitudMembresia>(solicitudesMembresia);
	}
	public static AtenderSolicitudMembresia[] getAllSolicitudMembresiaArray() {
		return solicitudesMembresia.toArray(new AtenderSolicitudMembresia[solicitudesMembresia.size()]);
	}

		// This Method only used in "Data Filter" Demo
		public static List<AtenderSolicitudMembresia> getFilterSolicitudMembresias(AtenderSolicitudMembresiaFilter solicitudesMembresiaFilter) {
			List<AtenderSolicitudMembresia> somesolicitudesMembresias = new ArrayList<AtenderSolicitudMembresia>();
			String cod = solicitudesMembresiaFilter.getCodigo().toLowerCase();
			String nom = solicitudesMembresiaFilter.getNombre().toLowerCase();
		
			for (Iterator<AtenderSolicitudMembresia> i = solicitudesMembresia.iterator(); i.hasNext();) {
				AtenderSolicitudMembresia tmp = i.next();
				if (tmp.getCodigo().toLowerCase().contains(cod) &&
						tmp.getNombre().toLowerCase().contains(nom)  ) {
					somesolicitudesMembresias.add(tmp);
				}
			}
			return somesolicitudesMembresias;
		}

		// This Method only used in "Header and footer" Demo
		public static List<AtenderSolicitudMembresia> getSolicitudesByCodigo(String codigo) {
			List<AtenderSolicitudMembresia> somesolicitudesMembresias = new ArrayList<AtenderSolicitudMembresia>();
			for (Iterator<AtenderSolicitudMembresia> i = solicitudesMembresia.iterator(); i.hasNext();) {
				AtenderSolicitudMembresia tmp = i.next();
				if (tmp.getCodigo().equalsIgnoreCase(codigo)){
					somesolicitudesMembresias.add(tmp);
				}
			}
			return somesolicitudesMembresias;
		}
}