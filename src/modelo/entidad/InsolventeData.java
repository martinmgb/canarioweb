package modelo.entidad;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import vistaModelo.InsolventeFilter;
import modelo.entidad.Insolvente;
public class InsolventeData {

	private static List<Insolvente> insolventes = new ArrayList<Insolvente>();
	static {
		insolventes.add(new Insolvente("21468751","Guillermo","Asaro","15/04/2015"));
		insolventes.add(new Insolvente("201467752","Fernando","Salazar","15/04/2015"));
		insolventes.add(new Insolvente("221478752","Aura","Mendoza","15/04/2015"));
		
	}
	//Retorna todos los Insolventes
	public static List<Insolvente> getTodosInsolventes() {
		return new ArrayList<Insolvente>(insolventes);
	}
	
	
    // Metodo que obtiene por filtro 
    public static List<Insolvente> getFiltroInsolvente(InsolventeFilter foodFilter) {
        List<Insolvente> someinsolvente = new ArrayList<Insolvente>();
        String cedula = foodFilter.getCedula().toLowerCase();
        String nombre = foodFilter.getNombre().toLowerCase();
        String apellido = foodFilter.getApellido().toLowerCase();
        for (Iterator<Insolvente> i = insolventes.iterator(); i.hasNext();) {
            Insolvente tmp = i.next();
            if (tmp.getCedula().toLowerCase().contains(cedula) 
            		&& tmp.getNombre().toLowerCase().contains(nombre)
            		&& tmp.getApellido().toLowerCase().contains(apellido)
            		) {
            	someinsolvente.add(tmp);
            }
        }
        return someinsolvente;
    }
    
    
    
   
}
