package modelo.entidad;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.ComisionFilter;
import vistaModelo.ComisionViewModel;
import modelo.entidad.Comision;

public class ComisionData {

	private static List<Comision> comisiones = new ArrayList<Comision>();
	static TipoComision tipoComision = new TipoComision("1","tipo","asd");
	static {
		comisiones.add(new Comision("001",tipoComision, "Tamarindo", "XXXX"));
		comisiones.add(new Comision("002",tipoComision, "Sala de Fiestas", "XXXX"));
		comisiones.add(new Comision("003",tipoComision, "Cancha de Bolas Criollas", "XXXX"));
		comisiones.add(new Comision("004",tipoComision, "Tamarindo", "XXXX"));
		comisiones.add(new Comision("005",tipoComision, "Piscina", "XXXX"));
		comisiones.add(new Comision("006",tipoComision, "Tamarindo", "XXXX"));
		comisiones.add(new Comision("007",tipoComision, "Piscina", "XXXX"));
		comisiones.add(new Comision("008",tipoComision, "Tamarindo", "XXXX"));
		comisiones.add(new Comision("009",tipoComision, "Piscina", "XXXX"));
		comisiones.add(new Comision("010",tipoComision, "Cancha de Bolas Criollas", "XXXX"));
		comisiones.add(new Comision("011",tipoComision, "Piscina", "XXXX"));
		comisiones.add(new Comision("012",tipoComision, "Tamarindo", "XXXX"));
		comisiones.add(new Comision("013",tipoComision, "Piscina", "XXXX"));
		comisiones.add(new Comision("014",tipoComision, "Cancha de Bolas Criollas", "XXXX"));
		comisiones.add(new Comision("015",tipoComision, "Piscina", "XXXX"));
		comisiones.add(new Comision("016",tipoComision, "Tamarindo", "XXXX"));
		comisiones.add(new Comision("017",tipoComision, "Piscina", "XXXX"));
		comisiones.add(new Comision("018",tipoComision, "Tamarindo", "XXXX"));
		comisiones.add(new Comision("019",tipoComision, "Piscina", "XXXX"));
		comisiones.add(new Comision("020",tipoComision, "Piscina", "XXXX"));
		comisiones.add(new Comision("021",tipoComision, "Sala de Fiestas", "XXXX"));
		comisiones.add(new Comision("022",tipoComision, "Piscina", "XXXX"));
	}

	public static List<Comision> getAllComisiones() {
		return new ArrayList<Comision>(comisiones);
	}
	public static Comision[] getAllComisionesArray() {
		return comisiones.toArray(new Comision[comisiones.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<Comision> getFilterComisiones(ComisionFilter filtroComision) {
		List<Comision> somecomisiones = new ArrayList<Comision>();
		String cod = filtroComision.getCodigo().toLowerCase();
		String nom = filtroComision.getNombre().toLowerCase();

		for (Iterator<Comision> i = comisiones.iterator(); i.hasNext();) {
			Comision tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getNombre().toLowerCase().contains(nom)) {
				somecomisiones.add(tmp);
			}
		}
		return somecomisiones;
	}

	// This Method only used in "Header and footer" Demo
	public static List<Comision> getComisionesByCodigo(String codigo) {
		List<Comision> somecomisiones = new ArrayList<Comision>();
		for (Iterator<Comision> i = comisiones.iterator(); i.hasNext();) {
			Comision tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				somecomisiones.add(tmp);
			}
		}
		return somecomisiones;
	}
}