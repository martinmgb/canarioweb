package modelo.entidad;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/

public class Cargo {
	private String codigo;
	private String tipo;
	private String nombre;
	;

	public Cargo(String codigo, String tipo,String nombre) {
		// TODO Auto-generated constructor stub
		this.codigo = codigo;
		this.tipo = tipo;
		this.nombre = nombre;
		
	}

	public Cargo() {
		// TODO Auto-generated constructor stub
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Comision [codigo=" + codigo + ", tipo=" + tipo + ", nombre=" + nombre
				+ " ]";
	}
	
	

}
