package modelo.entidad;

import java.util.Date;

public class Apelacion {
	private String codigo;
	private String persona;
	private String descripcion;
	private String fecha;

	

	
	public Apelacion(String codigo, String persona, String descripcion,
			String string) {
		super();
		this.codigo = codigo;
		this.persona = persona;
		this.descripcion = descripcion;
		this.fecha = string;
	}




	public String getCodigo() {
		return codigo;
	}




	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}




	public String getPersona() {
		return persona;
	}




	public void setPersona(String persona) {
		this.persona = persona;
	}




	public String getDescripcion() {
		return descripcion;
	}




	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}




	public String getFecha() {
		return fecha;
	}




	public void setFecha(String fecha) {
		this.fecha = fecha;
	}




	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Comision [codigo=" + codigo + ", persona=" + persona
				+ ", descripcion=" + descripcion + "fecha="+ fecha +"]";
	}
	
	

}
