package modelo.entidad;

public class Recurso {
	private String codigo;
	private String nombre;
	private String fechaingreso;
	private String cantidad;
	private String descripcion;

	public Recurso(String codigo, String nombre,String fechaingreso, String descripcion,String cantidad) {
		// TODO Auto-generated constructor stub
		this.codigo = codigo;
		this.nombre = nombre;
		this.fechaingreso = fechaingreso;
		this.descripcion = descripcion;
		this.cantidad = cantidad;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFechaingreso() {
		return fechaingreso;
	}

	public void setFechaingreso(String fechaingreso) {
		this.fechaingreso = fechaingreso;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	

}