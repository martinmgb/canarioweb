
package modelo.entidad;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import modelo.entidad.Accion;
import vistaModelo.AccionFilter;
 
public class AccionData{
 
    private static List<Accion> acciones = new ArrayList<Accion>();
    static {
        acciones.add(new Accion(1, "Martín Gutiérrez", new Date(),"imagen"));
        acciones.add(new Accion(2, "Martín Gutiérrez", new Date(),"imagen"));
        acciones.add(new Accion(3, "Martín Gutiérrez", new Date(),"imagen"));
        acciones.add(new Accion(4, "Martín Gutiérrez", new Date(),"imagen"));
        acciones.add(new Accion(5, "Martín Gutiérrez", new Date(),"imagen"));
        acciones.add(new Accion(6, "Martín Gutiérrez", new Date(),"imagen"));
        acciones.add(new Accion(7, "Monica Rodriguez", new Date(),"imagen"));
        acciones.add(new Accion(8, "Martín Gutiérrez", new Date(),"imagen"));
        acciones.add(new Accion(9, "Martín Gutiérrez", new Date(),"imagen"));
        acciones.add(new Accion(10, "Martín Gutiérrez", new Date(),"imagen"));
        acciones.add(new Accion(11, "Martín Gutiérrez", new Date(),"imagen"));
        acciones.add(new Accion(12, "Martín Gutiérrez", new Date(),"imagen"));
        acciones.add(new Accion(13, "Martín Gutiérrez", new Date(),"imagen"));
        acciones.add(new Accion(14, "Martín Gutiérrez", new Date(),"imagen"));
    }
 
    public static List<Accion> getTodasAcciones() {
        return new ArrayList<Accion>(acciones);
    }
    public static Accion[] getAllFoodsArray() {
        return acciones.toArray(new Accion[acciones.size()]);
    }
 
    // This Method only used in "Data Filter" Demo
    public static List<Accion> getFiltroAcciones(AccionFilter foodFilter) {
        List<Accion> someAreas = new ArrayList<Accion>();
        String miembro = foodFilter.getMiembro().toLowerCase();
         
        for (Iterator<Accion> i = acciones.iterator(); i.hasNext();) {
            Accion tmp = i.next();
            if (tmp.getMiembro().toLowerCase().contains(miembro)) {
                someAreas.add(tmp);
            }
        }
        return someAreas;
    }
 
    // This Method only used in "Header and footer" Demo
    public static List<Accion> getAccionesMiembro(String miembro) {
        List<Accion> someAreas = new ArrayList<Accion>();
        for (Iterator<Accion> i = acciones.iterator(); i.hasNext();) {
            Accion tmp = i.next();
            if (tmp.getMiembro().equalsIgnoreCase(miembro)){
                someAreas.add(tmp);
            }
        }
        return someAreas;
    }
}
