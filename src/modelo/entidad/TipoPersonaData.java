package modelo.entidad;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import vistaModelo.TipoPersonaViewModel;


public class TipoPersonaData {

	private static List<TipoPersona> personas = new ArrayList<TipoPersona>();
	static {
		personas.add(new TipoPersona("001", "Socio", "XXXX"));
		personas.add(new TipoPersona("002", "Beneficiario", "XXXX"));
		personas.add(new TipoPersona("003", "Cliente", "XXXX"));
		personas.add(new TipoPersona("004", "Directivo", "XXXX"));
	}

	public static List<TipoPersona> getAllPersonas() {
		return new ArrayList<TipoPersona>(personas);
	}
	public static TipoPersona[] getAllPersonasArray() {
		return personas.toArray(new TipoPersona[personas.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<TipoPersona> getFilterPersonas(TipoPersonaViewModel personaFilter) {
		List<TipoPersona> somepersonas = new ArrayList<TipoPersona>();
		String cod = personaFilter.getCodigo().toLowerCase();
		String tip = personaFilter.getTipo().toLowerCase();
		String des = personaFilter.getDescripcion().toLowerCase();

		for (Iterator<TipoPersona> i = personas.iterator(); i.hasNext();) {
			TipoPersona tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getTipo().toLowerCase().contains(tip)  &&
					tmp.getDescripcion().toLowerCase().contains(des)) {
				somepersonas.add(tmp);
			}
		}
		return somepersonas;
	}

	// This Method only used in "Header and footer" Demo
	public static List<TipoPersona> getPersonasByCodigo(String codigo) {
		List<TipoPersona> somepersonas = new ArrayList<TipoPersona>();
		for (Iterator<TipoPersona> i = personas.iterator(); i.hasNext();) {
			TipoPersona tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				somepersonas.add(tmp);
			}
		}
		return somepersonas;
	}
}