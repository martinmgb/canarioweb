package modelo.entidad;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/

public class Comision {
	private String codigo;
	private TipoComision tipocomision;
	private String nombre;
	private String descripcion;

	

	public Comision(String codigo, TipoComision tipocomision, String nombre,
			String descripcion) {
		super();
		this.codigo = codigo;
		this.tipocomision = tipocomision;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public Comision() {
		// TODO Auto-generated constructor stub
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	/**
	 * @return the tipocomision
	 */
	public TipoComision getTipocomision() {
		return tipocomision;
	}

	/**
	 * @param tipocomision the tipocomision to set
	 */
	public void setTipocomision(TipoComision tipocomision) {
		this.tipocomision = tipocomision;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Comision [codigo=" + codigo + ", nombre=" + nombre
				+ ", descripcion=" + descripcion + "]";
	}
	
	

}
