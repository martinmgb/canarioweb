package modelo.entidad;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import vistaModelo.AsistenciaEventoFilter;
import modelo.entidad.AsistenciaEvento;

public class AsistenciaEventoData {

	private static List<AsistenciaEvento> asistenciaevento = new ArrayList<AsistenciaEvento>();
	static {
		
		asistenciaevento.add(new AsistenciaEvento("20015150", "Gipsy", "Socio 1"));
		asistenciaevento.add(new AsistenciaEvento("20015154", "Nphely", "Socio 1"));
		asistenciaevento.add(new AsistenciaEvento("20156258", "Carlos", "Socio 2"));
		asistenciaevento.add(new AsistenciaEvento("20123456", "Antonio", "Socio 2"));
		asistenciaevento.add(new AsistenciaEvento("21125458", "Miguel", "Socio 3"));
		asistenciaevento.add(new AsistenciaEvento("25478888", "Andreina", "Socio 3"));

	}

	public static List<AsistenciaEvento> getAllEvento() {
		return new ArrayList<AsistenciaEvento>(asistenciaevento);
	}
	public static AsistenciaEvento[] getAllEventoArray() {
		return asistenciaevento.toArray(new AsistenciaEvento[asistenciaevento.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<AsistenciaEvento> getFilterEvento(AsistenciaEventoFilter eventoFilter) {
		List<AsistenciaEvento> someevento = new ArrayList<AsistenciaEvento>();
		String ced = eventoFilter.getCedula().toLowerCase();
		String nom = eventoFilter.getNombre().toLowerCase();
		String mie = eventoFilter.getmiembroAsociado().toLowerCase();

		for (Iterator<AsistenciaEvento> i = asistenciaevento.iterator(); i.hasNext();) {
			AsistenciaEvento tmp = i.next();
			if (tmp.getCedula().toLowerCase().contains(ced) &&
					tmp.getNombre().toLowerCase().contains(nom)  &&
					tmp.getMiembroAsociado().toLowerCase().contains(mie)) {
				someevento.add(tmp);
			}
		}
		return someevento;
	}

	// This Method only used in "Header and footer" Demo
	public static List<AsistenciaEvento> getEventoByCedula(String cedula) {
		List<AsistenciaEvento> someevento = new ArrayList<AsistenciaEvento>();
		for (Iterator<AsistenciaEvento> i = asistenciaevento.iterator(); i.hasNext();) {
			AsistenciaEvento tmp = i.next();
			if (tmp.getCedula().equalsIgnoreCase(cedula)){
				someevento.add(tmp);
			}
		}
		return someevento;
	}
}