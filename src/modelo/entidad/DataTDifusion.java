package modelo.entidad;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import modelo.entidad.TDifusion;

 
public class DataTDifusion {
 
    private static List<TDifusion> tdifusion = new ArrayList<TDifusion>();
    static {
    	tdifusion.add(new TDifusion(1, "Pública ", "XXXXX"));
        tdifusion.add(new TDifusion(2, "Privada", "XXXXX"));
    
     
   
      
    }
 
    public static List<TDifusion> getTodasTDifusion() {
        return new ArrayList<TDifusion>(tdifusion);
    }
    public static TDifusion[] getAllFoodsArray() {
        return tdifusion.toArray(new TDifusion[tdifusion.size()]);
    }
 
    // This Method only used in "Data Filter" Demo
    public static List<TDifusion> getFiltroTDifusion(vistaModelo.FiltroTDifusion filtroTDifusion) {
        List<TDifusion> someTDifusion = new ArrayList<TDifusion>();
        String nombre = filtroTDifusion.getNombre().toLowerCase();
        String descripcion = filtroTDifusion.getDescripcion().toLowerCase();
        List<TDifusion> someTDifusiones = new ArrayList<TDifusion>();
		for (Iterator<TDifusion> i = tdifusion.iterator(); i.hasNext();) {
        	TDifusion tmp = i.next();
            if (tmp.getNombre().toLowerCase().contains(nombre) 
            		&& tmp.getDescripcion().toLowerCase().contains(descripcion)) {
                someTDifusion.add(tmp);
            }
        }
        return someTDifusion;
    }
 
    // This Method only used in "Header and footer" Demo
    public static List<TDifusion> getNoticiaDescripcion(String descripcion) {
        List<TDifusion> someTDifusion= new ArrayList<TDifusion>();
        for (Iterator<TDifusion> i = tdifusion.iterator(); i.hasNext();) {
            TDifusion tmp = i.next();
            if (tmp.getDescripcion().equalsIgnoreCase(descripcion)){
                someTDifusion.add(tmp);
            }
        }
        return someTDifusion;
    }
    
    
}
