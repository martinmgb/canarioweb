package modelo.entidad;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import vistaModelo.ActividadFilter;
import modelo.entidad.Actividad;

public class ActividadData {

	private static List<Actividad> actividades = new ArrayList<Actividad>();
	static {
		actividades.add(new Actividad("001", "Junta Directiva"));
		actividades.add(new Actividad("002", "Secretaria"));
		actividades.add(new Actividad("003", "Mantenimiento"));
	}

	public static List<Actividad> getAllActividades() {
		return new ArrayList<Actividad>(actividades);
	}
	public static Actividad[] getAllActividadArray() {
		return actividades.toArray(new Actividad[actividades.size()]);
	}

		// This Method only used in "Data Filter" Demo
		public static List<Actividad> getFilterActividades(ActividadFilter actividadFilter) {
			List<Actividad> someactividades = new ArrayList<Actividad>();
			String cod = actividadFilter.getCodigo().toLowerCase();
			String des = actividadFilter.getDescripcion() .toLowerCase();
		
			for (Iterator<Actividad> i = actividades.iterator(); i.hasNext();) {
				Actividad tmp = i.next();
				if (tmp.getCodigo().toLowerCase().contains(cod) &&
						tmp.getDescripcion().toLowerCase().contains(des)  ) {
					someactividades.add(tmp);
				}
			}
			return someactividades;
		}

		// This Method only used in "Header and footer" Demo
		public static List<Actividad> getActividadesByCodigo(String codigo) {
			List<Actividad> someactividades = new ArrayList<Actividad>();
			for (Iterator<Actividad> i = actividades.iterator(); i.hasNext();) {
				Actividad tmp = i.next();
				if (tmp.getCodigo().equalsIgnoreCase(codigo)){
					someactividades.add(tmp);
				}
			}
			return someactividades;
		}
}