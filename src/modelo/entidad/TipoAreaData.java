package modelo.entidad;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import vistaModelo.TipoAreaFilter;
import modelo.entidad.TipoArea;

public class TipoAreaData {

	private static List<TipoArea> areas = new ArrayList<TipoArea>();
	static {
		areas.add(new TipoArea("001", "Tamarindo", "XXXX"));
		areas.add(new TipoArea("002", "Sala de Fiestas", "XXXX"));
		areas.add(new TipoArea("003", "Cancha de Bolas Criollas", "XXXX"));
		areas.add(new TipoArea("004", "Tamarindo", "XXXX"));
		areas.add(new TipoArea("005", "Piscina", "XXXX"));
		areas.add(new TipoArea("006", "Tamarindo", "XXXX"));
		areas.add(new TipoArea("007", "Piscina", "XXXX"));
		areas.add(new TipoArea("008", "Tamarindo", "XXXX"));
		areas.add(new TipoArea("009", "Piscina", "XXXX"));
		areas.add(new TipoArea("010", "Cancha de Bolas Criollas", "XXXX"));
		areas.add(new TipoArea("011", "Piscina", "XXXX"));
		areas.add(new TipoArea("012", "Tamarindo", "XXXX"));
		areas.add(new TipoArea("013", "Piscina", "XXXX"));
		areas.add(new TipoArea("014", "Cancha de Bolas Criollas", "XXXX"));
		areas.add(new TipoArea("015", "Piscina", "XXXX"));
		areas.add(new TipoArea("016", "Tamarindo", "XXXX"));
		areas.add(new TipoArea("017", "Piscina", "XXXX"));
		areas.add(new TipoArea("018", "Tamarindo", "XXXX"));
		areas.add(new TipoArea("019", "Piscina", "XXXX"));
		areas.add(new TipoArea("020", "Piscina", "XXXX"));
		areas.add(new TipoArea("021", "Sala de Fiestas", "XXXX"));
		areas.add(new TipoArea("022", "Piscina", "XXXX"));
	}

	public static List<TipoArea> getAllAreas() {
		return new ArrayList<TipoArea>(areas);
	}
	public static TipoArea[] getAllAreasArray() {
		return areas.toArray(new TipoArea[areas.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<TipoArea> getFilterAreas(TipoAreaFilter areaFilter) {
		List<TipoArea> someareas = new ArrayList<TipoArea>();
		String cod = areaFilter.getCodigo().toLowerCase();
		String tip = areaFilter.getTipo().toLowerCase();
		String des = areaFilter.getDescripcion().toLowerCase();

		for (Iterator<TipoArea> i = areas.iterator(); i.hasNext();) {
			TipoArea tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getTipo().toLowerCase().contains(tip)  &&
					tmp.getDescripcion().toLowerCase().contains(des)) {
				someareas.add(tmp);
			}
		}
		return someareas;
	}

	// This Method only used in "Header and footer" Demo
	public static List<TipoArea> getAreasByCodigo(String codigo) {
		List<TipoArea> someareas = new ArrayList<TipoArea>();
		for (Iterator<TipoArea> i = areas.iterator(); i.hasNext();) {
			TipoArea tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				someareas.add(tmp);
			}
		}
		return someareas;
	}
}