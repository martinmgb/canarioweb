package modelo.entidad;


public class TipoParentesco {
	private String codigo;
	private String nombre;
	

	

	public TipoParentesco(String codigo, String nombre) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
	}

	public TipoParentesco() {
		// TODO Auto-generated constructor stub
	}

	public String getCodigo() {
		return codigo;
	}

	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	@Override
	public String toString() {
		return "Tipo Parentescos [codigo=" + codigo + ", nombre=" + nombre
				+   "]";
	}
	
	

}
