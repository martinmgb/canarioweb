package modelo.entidad;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
 
import modelo.entidad.UnidadMedida;
import vistaModelo.UnidadMedidaFilter;
 
public class UnidadMedidaData {
 
    private static List<UnidadMedida> unidades = new ArrayList<UnidadMedida>();
    static {
    	unidades.add(new UnidadMedida("001", "Medida 1"));
    	unidades.add(new UnidadMedida("002", "Medida 2"));
    	unidades.add(new UnidadMedida("003", "Medida 3"));
    	unidades.add(new UnidadMedida("004", "Medida 4"));
    	unidades.add(new UnidadMedida("005", "Medida 5"));
    	unidades.add(new UnidadMedida("006", "Medida 6"));
    	unidades.add(new UnidadMedida("007", "Medida 7"));
    	unidades.add(new UnidadMedida("008", "Medida 8"));
    	unidades.add(new UnidadMedida("009", "Medida 9"));
    	unidades.add(new UnidadMedida("010", "Medida 10"));
    	unidades.add(new UnidadMedida("011", "Medida 11"));
    	unidades.add(new UnidadMedida("012", "Medida 12"));
    	unidades.add(new UnidadMedida("013", "Medida 13"));
        
    }
 
    public static List<UnidadMedida> getTodasUnidades() {
        return new ArrayList<UnidadMedida>(unidades);
    }
    public static TipoArea[] getAllFoodsArray() {
        return unidades.toArray(new TipoArea[unidades.size()]);
    }
 
    // This Method only used in "Data Filter" Demo
    public static List<UnidadMedida> getFiltroUnidades(UnidadMedidaFilter Filter) {
        List<UnidadMedida> someUnidades = new ArrayList<UnidadMedida>();
        String nombre = Filter.getNombre().toLowerCase();
        String codigo = Filter.getCodigo().toLowerCase();
        
        for (Iterator<UnidadMedida> i = unidades.iterator(); i.hasNext();) {
            UnidadMedida tmp = i.next();
            if (tmp.getNombre().toLowerCase().contains(nombre) 
            		&& tmp.getCodigo().toLowerCase().contains(codigo)
            		) {
                someUnidades.add(tmp);
            }
        }
        return someUnidades;
    }
 
    // This Method only used in "Header and footer" Demo
    public static List<UnidadMedida> getUnidadesNombre(String nombre) {
        List<UnidadMedida> someUnidades = new ArrayList<UnidadMedida>();
        for (Iterator<UnidadMedida> i = unidades.iterator(); i.hasNext();) {
            UnidadMedida tmp = i.next();
            if (tmp.getNombre().equalsIgnoreCase(nombre)){
                someUnidades.add(tmp);
            }
        }
        return someUnidades;
    }
    
    public static List<UnidadMedida> getUnidadesCodigo(String nombre) {
        List<UnidadMedida> someUnidades = new ArrayList<UnidadMedida>();
        for (Iterator<UnidadMedida> i = unidades.iterator(); i.hasNext();) {
            UnidadMedida tmp = i.next();
            if (tmp.getCodigo().equalsIgnoreCase(nombre)){
                someUnidades.add(tmp);
            }
        }
        return someUnidades;
    }
    
  
}
