package modelo.entidad;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.ComisionPersonaFilter;
import vistaModelo.ComisionPersonaViewModel;
import modelo.entidad.ComisionPersona;

public class ComisionPersonaData {

	private static List<ComisionPersona> comper = new ArrayList<ComisionPersona>();
	static TipoComision tipoComision = new TipoComision("1","tipo","asd");
	static Comision comision = new Comision("001",tipoComision, "Tamarindo", "XXXX");
	static Cliente persona = new Cliente("21468751","Guillermo","guillermo123@gmail.com","04245368566","c","c","c","c","c");
	static {
		comper.add(new ComisionPersona(comision,persona));
		comper.add(new ComisionPersona(comision,persona));
	}

	public static List<ComisionPersona> getAllComPer() {
		return new ArrayList<ComisionPersona>(comper);
	}
	public static ComisionPersona[] getAllComPerArray() {
		return comper.toArray(new ComisionPersona[comper.size()]);
	}

//	// This Method only used in "Data Filter" Demo
//	public static List<ComisionPersona> getFilterComPer(ComisionPersonaFilter filtroComisionPersona) {
//		List<ComisionPersona> somecomper = new ArrayList<ComisionPersona>();
//		String cod = filtroComisionPersona.getCodigo().toLowerCase();
//		String nom = filtroComisionPersona.getNombre().toLowerCase();
//
//		for (Iterator<ComisionPersona> i = comper.iterator(); i.hasNext();) {
//			ComisionPersona tmp = i.next();
//			if (tmp.getCodigo().toLowerCase().contains(cod) &&
//					tmp.getNombre().toLowerCase().contains(nom)) {
//				somecomper.add(tmp);
//			}
//		}
//		return somecomper;
//	}
//
//	// This Method only used in "Header and footer" Demo
//	public static List<ComisionPersona> getComPerByCodigo(String codigo) {
//		List<ComisionPersona> somecomper = new ArrayList<ComisionPersona>();
//		for (Iterator<ComisionPersona> i = comper.iterator(); i.hasNext();) {
//			ComisionPersona tmp = i.next();
//			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
//				somecomper.add(tmp);
//			}
//		}
//		return somecomper;
//	}
}