package modelo.entidad;

public class AprobarPagoArrendamiento {

	private String fechapago;
	private String montopago;
	private String resto;
	private String area;


	public AprobarPagoArrendamiento(String fechapago, String area,String montopago, String resto) {
		super();
		this.fechapago = fechapago;
		this.area =area;
		this.montopago = montopago;
		this.resto = resto;
	}


	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}


	public String getFechapago() {
		return fechapago;
	}
	public void setFechapago(String fechapago) {
		this.fechapago = fechapago;
	}
	public String getMontopago() {
		return montopago;
	}
	public void setMontopago(String montopago) {
		this.montopago = montopago;
	}
	public String getResto() {
		return resto;
	}
	public void setResto(String resto) {
		this.resto = resto;
	}



}
