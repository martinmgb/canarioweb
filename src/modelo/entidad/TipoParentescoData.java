package modelo.entidad;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.TipoParentescoFilter;
import vistaModelo.TipoParentescoViewModel;
import modelo.entidad.TipoParentesco;

public class TipoParentescoData {

	private static List<TipoParentesco> tiposparentesco = new ArrayList<TipoParentesco>();
	static {
		tiposparentesco.add(new TipoParentesco("001", "Hijo"));
		tiposparentesco.add(new TipoParentesco("002", "Esposa"));
		tiposparentesco.add(new TipoParentesco("003", "Nieta"));
		tiposparentesco.add(new TipoParentesco("004", "Suegra"));
		
	}

	public static List<TipoParentesco> getAllTipoParentescoes() {
		return new ArrayList<TipoParentesco>(tiposparentesco);
	}
	public static TipoParentesco[] getAllTipoParentescoesArray() {
		return tiposparentesco.toArray(new TipoParentesco[tiposparentesco.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<TipoParentesco> getFilterTipoParentescoes(TipoParentescoFilter filtroTipoParentesco) {
		List<TipoParentesco> sometiposparentesco = new ArrayList<TipoParentesco>();
		String cod = filtroTipoParentesco.getCodigo().toLowerCase();
		String nom = filtroTipoParentesco.getNombre().toLowerCase();

		for (Iterator<TipoParentesco> i = tiposparentesco.iterator(); i.hasNext();) {
			TipoParentesco tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getNombre().toLowerCase().contains(nom)) {
				sometiposparentesco.add(tmp);
			}
		}
		return sometiposparentesco;
	}

	// This Method only used in "Header and footer" Demo
	public static List<TipoParentesco> getTipoParentescoesByCodigo(String codigo) {
		List<TipoParentesco> sometiposparentesco = new ArrayList<TipoParentesco>();
		for (Iterator<TipoParentesco> i = tiposparentesco.iterator(); i.hasNext();) {
			TipoParentesco tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				sometiposparentesco.add(tmp);
			}
		}
		return sometiposparentesco;
	}
}