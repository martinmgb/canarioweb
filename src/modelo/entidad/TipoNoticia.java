package modelo.entidad;
/*creado por Evelin Perez
fecha 14/06/2015*/
import java.io.Serializable;

public class TipoNoticia implements  Serializable{
	
	private static final long serialVersionUID = 1L;
	private boolean editingStatus=false;
	private char estatus= 'A';
	

	private String codigo;
	private String nombre;
	
	
	 public TipoNoticia() {
			super();
			// TODO Auto-generated constructor stub
		}
	 
	 public TipoNoticia(String codigo, String nombre)
	 {
			super();
			this.setCodigo(codigo);
			this.setNombre(nombre);
			
		}
	 public boolean isEditingStatus() {
			return editingStatus;
		}
		public void setEditingStatus(boolean editingStatus) {
			this.editingStatus = editingStatus;
		}
	 public char getEstatus() {
			return estatus;
		}
		public void setEstatus(char estatus) {
			this.estatus = estatus;
		}

		public String getCodigo() {
			return codigo;
		}

		public void setCodigo(String codigo) {
			this.codigo = codigo;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

}
