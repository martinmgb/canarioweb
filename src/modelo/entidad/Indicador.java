package modelo.entidad;
/*creado por Aura Manzanilla
fecha 14/06/2015*/
import java.io.Serializable;

public class Indicador implements  Serializable{
	
	private static final long serialVersionUID = 1L;
	private boolean editingStatus=false;
	private char estatus= 'A';
	

	private String codigo;
	private String nombre;
	private String tipo;
	private String unidad;
	
	 public Indicador() {
			super();
			// TODO Auto-generated constructor stub
		}
	 
	 public Indicador(String codigo, String nombre, String tipo, String unidad)
	 {
			super();
			this.setCodigo(codigo);
			this.setNombre(nombre);
			this.setTipo(tipo);
			this.setUnidad(unidad);
		}
	 public boolean isEditingStatus() {
			return editingStatus;
		}
		public void setEditingStatus(boolean editingStatus) {
			this.editingStatus = editingStatus;
		}
	 public char getEstatus() {
			return estatus;
		}
		public void setEstatus(char estatus) {
			this.estatus = estatus;
		}

		public String getCodigo() {
			return codigo;
		}

		public void setCodigo(String codigo) {
			this.codigo = codigo;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getTipo() {
			return tipo;
		}

		public void setTipo(String tipo) {
			this.tipo = tipo;
		}

		public String getUnidad() {
			return unidad;
		}

		public void setUnidad(String unidad) {
			this.unidad = unidad;
		}
}
