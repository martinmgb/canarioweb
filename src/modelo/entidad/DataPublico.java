package modelo.entidad;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import modelo.entidad.Publico;
import modelo.entidad.FiltroPublico;
 
public class DataPublico {
 
    private static List<Publico> publico = new ArrayList<Publico>();
    static {
    	
    	publico.add(new Publico(1, "Infantil", "XXXXX"));
        publico.add(new Publico(2,"Jóvenes", "XXXXX"));
        publico.add(new Publico(3, "Adultos", "XXXXX	"));
        publico.add(new Publico(3, "Tercera Edad", "XXXXX	"));
    
    }
 
    public static List<Publico> getTodoPublico() {
        return new ArrayList<Publico>(publico);
    }
    public static Publico[] getAllFoodsArray() {
        return publico.toArray(new Publico[publico.size()]);
    }
 
    // This Method only used in "Data Filter" Demo
    public static List<Publico> getFiltroPublico(vistaModelo.FiltroPublico filtroPublico) {
        List<Publico> somePublico = new ArrayList<Publico>();
        String nombre = filtroPublico.getNombre().toLowerCase();
        String descripcion = filtroPublico.getDescripcion().toLowerCase();
        
        
        List<Publico> somePublicos;
		for (Iterator<Publico> i = publico.iterator(); i.hasNext();) {
        	Publico tmp = i.next();
            if (tmp.getNombre().toLowerCase().contains(nombre) 
            		&& tmp.getDescripcion().toLowerCase().contains(descripcion)) {
                somePublico.add(tmp);
            }
        }
        return somePublico;
    }
 
    // This Method only used in "Header and footer" Demo
    public static List<Publico> getPublicoDescripcion(String descripcion) {
        List<Publico> somePublico= new ArrayList<Publico>();
        for (Iterator<Publico> i = publico.iterator(); i.hasNext();) {
            Publico tmp = i.next();
            if (tmp.getDescripcion().equalsIgnoreCase(descripcion)){
                somePublico.add(tmp);
            }
        }
        return somePublico;
    }
    
    
}
