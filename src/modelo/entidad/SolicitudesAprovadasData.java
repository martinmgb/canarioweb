package modelo.entidad;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.AprovadaFilter;
import vistaModelo.InsolventeFilter;

public class SolicitudesAprovadasData {

	private static List<SolicitudesAprovadas> aprovadas = new ArrayList<SolicitudesAprovadas>();
	static {
		aprovadas.add(new SolicitudesAprovadas("001","20469750","Guillermo","Asaro","Miembro","Bosquesito","15/07/2015","5000","2500"));
	}
	//Retorna todos las Solicitudes
	public static List<SolicitudesAprovadas> getTodasSolicitudesAprovadas() {
		return new ArrayList<SolicitudesAprovadas>(aprovadas);
	}

    // Metodo que obtiene por filtro 
    public static List<SolicitudesAprovadas> getFilterSolicitudes(AprovadaFilter foodFilter) {
        List<SolicitudesAprovadas> someaprovadas = new ArrayList<SolicitudesAprovadas>();
        String codigo = foodFilter.getCodigo().toLowerCase();
        String nombre = foodFilter.getNombre().toLowerCase();
        String cedula = foodFilter.getCedula().toLowerCase();
        String apellido = foodFilter.getApellido().toLowerCase();
        String tipo = foodFilter.getApellido().toLowerCase();
        for (Iterator<SolicitudesAprovadas> i = aprovadas.iterator(); i.hasNext();) {
        	SolicitudesAprovadas tmp = i.next();
            if (tmp.getCodigo().toLowerCase().contains(codigo) 
            		&& tmp.getNombre().toLowerCase().contains(nombre)
            		&& tmp.getCedula().toLowerCase().contains(cedula)
            		&& tmp.getApellido().toLowerCase().contains(apellido)
            		) {
            	someaprovadas.add(tmp);
            }
        }
        return someaprovadas;
    }
    
    
    	
	
	
}
