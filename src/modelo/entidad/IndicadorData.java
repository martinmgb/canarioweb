package modelo.entidad;
/*creado por Aura Manzanilla
fecha 14/06/2015*/
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.IndicadorFilter;

public class IndicadorData {
	 private static List<Indicador> indicadores = new ArrayList<Indicador>();
	 static {
		 indicadores.add(new Indicador("1", "Satisfaccion de Miembros", "Satisfaccion", "Porcentaje"));
		 indicadores.add(new Indicador("2", "Ganancias", "Gastos-Ingresos", "Bs"));
	 }

		public static List<Indicador> getAllIndicadores() {
			return new ArrayList<Indicador>(indicadores);
		}
		public static Indicador[] getAllIndicadoresArray() {
			return indicadores.toArray(new Indicador[indicadores.size()]);
		}

		// This Method only used in "Data Filter" Demo
		public static List<Indicador> getFilterIndicadores(IndicadorFilter filtroIndicador) {
			List<Indicador> someindicadores = new ArrayList<Indicador>();
			String cod = filtroIndicador.getCodigo().toLowerCase();
			String nom = filtroIndicador.getNombre().toLowerCase();
			String tipo = filtroIndicador.getTipo().toLowerCase();
			String uni = filtroIndicador.getUnidad().toLowerCase();

			for (Iterator<Indicador> i = indicadores.iterator(); i.hasNext();) {
				Indicador tmp = i.next();
				if (tmp.getCodigo().toLowerCase().contains(cod) &&
						tmp.getNombre().toLowerCase().contains(nom)&&
						tmp.getTipo().toLowerCase().contains(tipo)&&
						tmp.getUnidad().toLowerCase().contains(uni)) {
					someindicadores.add(tmp);
				}
			}
			return someindicadores;
		}

		// This Method only used in "Header and footer" Demo
		public static List<Indicador> getIndicadoresByCodigo(String codigo) {
			List<Indicador> someindicadores = new ArrayList<Indicador>();
			for (Iterator<Indicador> i = indicadores.iterator(); i.hasNext();) {
				Indicador tmp = i.next();
				if (tmp.getCodigo().equalsIgnoreCase(codigo)){
					someindicadores.add(tmp);
				}
			}
			return someindicadores;
		}
	}