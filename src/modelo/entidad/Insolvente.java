package modelo.entidad;

import java.io.Serializable;
import java.sql.Date;

public class Insolvente implements  Serializable{

	private static final long serialVersionUID = 1L;
	private String cedula;
	private String nombre;
	private String Apellido;
	private String  fechavencimiento;


	public Insolvente() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Insolvente(String cedula, String nombre, String apellido,
		 String fechavencimiento) {
		super();
		this.cedula = cedula;
		this.nombre = nombre;
		Apellido = apellido;
		this.fechavencimiento = fechavencimiento;
	}

	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFechavencimiento() {
		return fechavencimiento;
	}
	public void setFechavencimiento(String fechavencimiento) {
		this.fechavencimiento = fechavencimiento;
	}

	public String getApellido() {
		return Apellido;
	}
	public void setApellido(String apellido) {
		Apellido = apellido;
	}

}
