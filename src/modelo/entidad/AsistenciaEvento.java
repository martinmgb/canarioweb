package modelo.entidad;

public class AsistenciaEvento {
	private String cedula;
	private String nombre;
	private String miembroAsociado;
	
	public AsistenciaEvento(String cedula, String nombre, String miembroAsociado) {
		// TODO Auto-generated constructor stub
		this.cedula = cedula;
		this.nombre = nombre;
		this.miembroAsociado = miembroAsociado;
	}
	
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMiembroAsociado() {
		return miembroAsociado;
	}
	public void setMiembroAsociado(String miembroAsociado) {
		this.miembroAsociado = miembroAsociado;
	}

	
	

}
