package modelo.entidad;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.NoticiaFilter;

public class NoticiaData {
	private static List<Noticia> noticias = new ArrayList<Noticia>();
	static {
		noticias.add(new Noticia("N001", "Deportiva","Todo Publico", "assets/images/noticia1.jpg","Comienza el XVI Torneo de Tenis Sampras","Roland Garros fue el primer Grand Slam en unirse a la Era Open en 1968, y desde entonces muchas figuras mundiales han engalanado las famosas canchas de arcilla, como Bjorn Borg, Ivan Lendl, Mats Wilander y Gustavo Kuerten. Los aficionados locales nunca olvidarán 1983, cuando Yannick Noah fue el primer, y hasta ahora único, francés en ganar el título de singles. En 2013, Nadal se convirtió en el primer jugador en ganar ocho títulos de un mismo Grand Slam."));
		noticias.add(new Noticia("N002", "General","Todo Publico", "assets/images/noticia2.jpg","Piscina Clausurada","Nuestra piscina fue clausurada por la escasez de quìmicos para su mantenimiento y aprovechando esta situaciòn se decidiò reparar las fallas estructurales. Por orden del alcalde Josy Fernàndez, preocupado por la salud de los atletas, se cerrò la alberca y la obra se ejecutò a toda màquina, pensando en el bienestar de todos los usuarios y a partir del 24 al 27 de julio la pileta estarà cerrada para cambiar los filtros"));
		noticias.add(new Noticia("N003", "Eventos","Solo Miembros", "assets/images/noticia3.jpg","Celebra el Dia de las Madres con nosotros","Este domingo las madres  celebrarán su día, y como es de esperarse todos los famosos ya han colgado fotografías referente a este gran celebración. El Día de la Madre o Día de las Madres es una festividad que se celebra en honor a ellas en diferentes fechas del año, aunque madre hay una sola y no se necesita un día especifico para recordarle lo maravillosa que es."));
	}

	public static List<Noticia> getAllNoticias() {
		return new ArrayList<Noticia>(noticias);
	}
	public static Noticia[] getAllNoticiasArray() {
		return noticias.toArray(new Noticia[noticias.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<Noticia> getFilterNoticias(NoticiaFilter noticiaFilter) {
		List<Noticia> somenoticias = new ArrayList<Noticia>();
		String cod = noticiaFilter.getCodigo().toLowerCase();
		String tip = noticiaFilter.getTipo().toLowerCase();
		String dif = noticiaFilter.getDifusion().toLowerCase();
		String tit = noticiaFilter.getTitulo().toLowerCase();

		for (Iterator<Noticia> i = noticias.iterator(); i.hasNext();) {
			Noticia tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getTipo().toLowerCase().contains(tip)  &&
					tmp.getDifusion().toLowerCase().contains(dif)  &&
					tmp.getTitulo().toLowerCase().contains(tit)) {
				somenoticias.add(tmp);
			}
		}
		return somenoticias;
	}

	// This Method only used in "Header and footer" Demo
	public static List<Noticia> getNoticiaByCodigo(String codigo) {
		List<Noticia> somenoticias = new ArrayList<Noticia>();
		for (Iterator<Noticia> i = noticias.iterator(); i.hasNext();) {
			Noticia tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				somenoticias.add(tmp);
			}
		}
		return somenoticias;
	}
}
