package modelo.entidad;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.ComisionActividadFilter;
import vistaModelo.ComisionActividadViewModel;
import modelo.entidad.ComisionActividad;

public class ComisionActividadData {

	private static List<ComisionActividad> comact = new ArrayList<ComisionActividad>();
	static TipoComision tipoComision = new TipoComision("1","tipo","asd");
	static Comision comision = new Comision("001",tipoComision, "Tamarindo", "XXXX");
	static Actividad actividad = new Actividad("001", "Junta Directiva");
	static {
		comact.add(new ComisionActividad(comision,actividad));
		comact.add(new ComisionActividad(comision,actividad));
	}

	public static List<ComisionActividad> getAllComAct() {
		return new ArrayList<ComisionActividad>(comact);
	}
	public static ComisionActividad[] getAllComActArray() {
		return comact.toArray(new ComisionActividad[comact.size()]);
	}

//	// This Method only used in "Data Filter" Demo
//	public static List<ComisionActividad> getFilterComAct(ComisionActividadFilter filtroComisionActividad) {
//		List<ComisionActividad> somecomact = new ArrayList<ComisionActividad>();
//		String cod = filtroComisionActividad.getCodigo().toLowerCase();
//		String nom = filtroComisionActividad.getNombre().toLowerCase();
//
//		for (Iterator<ComisionActividad> i = comact.iterator(); i.hasNext();) {
//			ComisionActividad tmp = i.next();
//			if (tmp.getCodigo().toLowerCase().contains(cod) &&
//					tmp.getNombre().toLowerCase().contains(nom)) {
//				somecomact.add(tmp);
//			}
//		}
//		return somecomact;
//	}
//
//	// This Method only used in "Header and footer" Demo
//	public static List<ComisionActividad> getComActByCodigo(String codigo) {
//		List<ComisionActividad> somecomact = new ArrayList<ComisionActividad>();
//		for (Iterator<ComisionActividad> i = comact.iterator(); i.hasNext();) {
//			ComisionActividad tmp = i.next();
//			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
//				somecomact.add(tmp);
//			}
//		}
//		return somecomact;
//	}
}