package modelo.entidad;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/

public class ComisionActividad {
	private Comision comision;
	private Actividad actividad;
	
	public ComisionActividad(Comision comision, Actividad actividad) {
		super();
		this.comision = comision;
		this.actividad = actividad;
	}

	public ComisionActividad() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the comision
	 */
	public Comision getComision() {
		return comision;
	}

	/**
	 * @param comision the comision to set
	 */
	public void setComision(Comision comision) {
		this.comision = comision;
	}

	/**
	 * @return the actividad
	 */
	public Actividad getActividad() {
		return actividad;
	}

	/**
	 * @param actividad the actividad to set
	 */
	public void setActividad(Actividad actividad) {
		this.actividad = actividad;
	}
	
	
}
