package modelo.entidad;
/*creado por Evelin Perez
fecha 14/06/2015*/
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.TipoNoticiaFilter;

public class TipoNoticiaData {
	 private static List<TipoNoticia> tiponoticia = new ArrayList<TipoNoticia>();
	 static {
		 tiponoticia.add(new TipoNoticia("1", "Social"));
		 tiponoticia.add(new TipoNoticia("2", "Cultural"));
		 tiponoticia.add(new TipoNoticia("3", "Deportiva"));
	 }

		public static List<TipoNoticia> getAllTipoNoticia() {
			return new ArrayList<TipoNoticia>(tiponoticia);
		}
		public static TipoNoticia[] getAllTipoNoticiaArray() {
			return tiponoticia.toArray(new TipoNoticia[tiponoticia.size()]);
		}

		// This Method only used in "Data Filter" Demo
		public static List<TipoNoticia> getFilterTipoNoticia(TipoNoticiaFilter filtroTipoNoticia) {
			List<TipoNoticia> sometiponoticia = new ArrayList<TipoNoticia>();
			String cod = filtroTipoNoticia.getCodigo().toLowerCase();
			String nom = filtroTipoNoticia.getNombre().toLowerCase();

			for (Iterator<TipoNoticia> i = tiponoticia.iterator(); i.hasNext();) {
				TipoNoticia tmp = i.next();
				if (tmp.getCodigo().toLowerCase().contains(cod) &&
						tmp.getNombre().toLowerCase().contains(nom)) {
					sometiponoticia.add(tmp);
				}
			}
			return sometiponoticia;
		}

		// This Method only used in "Header and footer" Demo
		public static List<TipoNoticia> getTipoNoticiaByCodigo(String codigo) {
			List<TipoNoticia> sometiponoticia = new ArrayList<TipoNoticia>();
			for (Iterator<TipoNoticia> i = tiponoticia.iterator(); i.hasNext();) {
				TipoNoticia tmp = i.next();
				if (tmp.getCodigo().equalsIgnoreCase(codigo)){
					sometiponoticia.add(tmp);
				}
			}
			return sometiponoticia;
		}
	}