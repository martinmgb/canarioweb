package modelo.entidad;
/*creado por Aura Manzanilla 19669350
fecha 23/06/2015*/
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.CargoFilter;
import modelo.entidad.Cargo;



public class CargoData {

	private static List<Cargo> cargos = new ArrayList<Cargo>();
	static {
		cargos.add(new Cargo("001", "Junta Directiva", "Presidente"));
		cargos.add(new Cargo("002", "Junta Directiva", "Viceprecidente"));
		cargos.add(new Cargo("003", "Junta Directiva", "Primer suplente"));
		cargos.add(new Cargo("004", "Junta Directiva", "Director de Fiestas"));
		cargos.add(new Cargo("005", "Administrativo", "Secretaria"));
		
	}

	public static List<Cargo> getAllCargos() {
		return new ArrayList<Cargo>(cargos);
	}
	public static Cargo[] getAllCargosArray() {
		return cargos.toArray(new Cargo[cargos.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<Cargo> getFilterCargos(CargoFilter filtroCargo) {
		List<Cargo> somecargos = new ArrayList<Cargo>();
		String cod = filtroCargo.getCodigo().toLowerCase();
		String tip = filtroCargo.getTipo().toLowerCase();
		String nom = filtroCargo.getNombre().toLowerCase();

		for (Iterator<Cargo> i = cargos.iterator(); i.hasNext();) {
			Cargo tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) && tmp.getTipo().toLowerCase().contains(tip) &&
					tmp.getNombre().toLowerCase().contains(nom)) {
				somecargos.add(tmp);
			}
		}
		return somecargos;
	}

	// This Method only used in "Header and footer" Demo
	public static List<Cargo> getCargosByCodigo(String codigo) {
		List<Cargo> somecargos = new ArrayList<Cargo>();
		for (Iterator<Cargo> i = cargos.iterator(); i.hasNext();) {
			Cargo tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				somecargos.add(tmp);
			}
		}
		return somecargos;
	}
}