package modelo.entidad;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import vistaModelo.TipoSancionFilter;
import modelo.entidad.TipoSancion;

public class TipoSancionData {

	private static List<TipoSancion> sanciones = new ArrayList<TipoSancion>();
	static {
		sanciones.add(new TipoSancion("001", "SANCION1", "XXXX"));
		sanciones.add(new TipoSancion("002", "SANCION2", "XXXX"));
		sanciones.add(new TipoSancion("003", "SANCION3", "XXXX"));
		sanciones.add(new TipoSancion("004", "SANCION4", "XXXX"));
	}

	public static List<TipoSancion> getAllSanciones() {
		return new ArrayList<TipoSancion>(sanciones);
	}
	public static TipoSancion[] getAllSancionesArray() {
		return sanciones.toArray(new TipoSancion[sanciones.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<TipoSancion> getFilterSanciones(TipoSancionFilter sancionFilter) {
		List<TipoSancion> somesanciones = new ArrayList<TipoSancion>();
		String cod = sancionFilter.getCodigo().toLowerCase();
		String tip = sancionFilter.getTipo().toLowerCase();
		String des = sancionFilter.getDescripcion().toLowerCase();

		for (Iterator<TipoSancion> i = sanciones.iterator(); i.hasNext();) {
			TipoSancion tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getTipo().toLowerCase().contains(tip)  &&
					tmp.getDescripcion().toLowerCase().contains(des)) {
				somesanciones.add(tmp);
			}
		}
		return somesanciones;
	}

	// This Method only used in "Header and footer" Demo
	public static List<TipoSancion> getSancionesByCodigo(String codigo) {
		List<TipoSancion> somesanciones = new ArrayList<TipoSancion>();
		for (Iterator<TipoSancion> i = sanciones.iterator(); i.hasNext();) {
			TipoSancion tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				somesanciones.add(tmp);
			}
		}
		return somesanciones;
	}
}