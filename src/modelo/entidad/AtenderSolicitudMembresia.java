package modelo.entidad;

public class AtenderSolicitudMembresia {
	private String codigo;
	private String nombre;
	

	public AtenderSolicitudMembresia(String codigo, String nombre) {
		// TODO Auto-generated constructor stub
		this.codigo = codigo;
		this.nombre = nombre;
		
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String toString() {
		return "AtenderSolicitudMembresia [codigo=" + codigo + ", nombre=" + nombre
				+  "]";
	}

	

}
