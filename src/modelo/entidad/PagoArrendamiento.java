package modelo.entidad;

public class PagoArrendamiento extends SolicitudesAprovadas {
	private String fechapago;
	private String descripcion;

	public PagoArrendamiento() {
		super();
		// TODO Auto-generated constructor stub
	}




	public PagoArrendamiento(String codigo, String cedula, String nombre,
			String apellido, String tipo, String area, String fecha,
			String monto, String montopagado, String fechapago,
			String descripcion) {
		super(codigo, cedula, nombre, apellido, tipo, area, fecha, monto,
				montopagado);
		this.fechapago = fechapago;
		this.descripcion = descripcion;
	}




	public String getFechapago() {
		return fechapago;
	}

	public void setFechapago(String fechapago) {
		this.fechapago = fechapago;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
