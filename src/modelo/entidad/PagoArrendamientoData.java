package modelo.entidad;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.AprovadaFilter;
import vistaModelo.PagoArrendamientoFilter;
import vistaModelo.RecursoFilter;

public class PagoArrendamientoData {
	
	private static List<PagoArrendamiento > pagoarrendamiento = new ArrayList<PagoArrendamiento>();
	static {
		pagoarrendamiento.add(new PagoArrendamiento("001","20469751", "Guillermo","Azaro","miembro","piscina","25/06/2015","2500","2500","25/06/2015","Primer Pago"));
		
	}
	//Retorna todos los pagos realizado por el usuario
	public static List<PagoArrendamiento> getAllPagosArrendamiento() {
		return new ArrayList<PagoArrendamiento>(pagoarrendamiento);
	}
	
	   // Metodo que obtiene por filtro 
	public static List<PagoArrendamiento> getFilterPagosArrendamiento(PagoArrendamientoFilter foodFilter) {
        List<PagoArrendamiento> someapagosarrendamiento = new ArrayList<PagoArrendamiento>();
        String cedula = foodFilter.getCedula().toLowerCase();
        for (Iterator<PagoArrendamiento> i = pagoarrendamiento.iterator(); i.hasNext();) {
        	PagoArrendamiento tmp = i.next();
            if (tmp.getCedula().toLowerCase().contains(cedula)){
            	someapagosarrendamiento.add(tmp);
            		
            }
        }
        return someapagosarrendamiento;   
            }
        }
	

