package modelo.entidad;

public class Noticia {
	private String codigo;
	private String tipo;
	private String difusion;
	private String imagen;
	private String titulo;
	private String descripcion;

	public Noticia(String codigo, String tipo, String difusion, String imagen,
			String titulo, String descripcion) {
		super();
		this.codigo = codigo;
		this.tipo = tipo;
		this.difusion = difusion;
		this.imagen = imagen;
		this.titulo = titulo;
		this.descripcion = descripcion;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDifusion() {
		return difusion;
	}
	public void setDifusion(String difusion) {
		this.difusion = difusion;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
