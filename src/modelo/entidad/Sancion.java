package modelo.entidad;



public class Sancion {
	private String codigo;
	private String tipo;
	private String descripcion;
	private String fecha;
	private String cedula;
	private String nombre;
	private String codIncidencia;
	public Sancion(String codigo, String tipo, String descripcion, String fecha, String cedula,String nombre, String codIncidencia) {
		super();
		this.codigo = codigo;
		this.tipo = tipo;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.cedula = cedula;
		this.nombre = nombre; 
		this.codIncidencia = codIncidencia;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCodIncidencia() {
		return codIncidencia;
	}
	public void setCodIncidencia(String codIncidencia) {
		this.codIncidencia = codIncidencia;
	}
	
}
