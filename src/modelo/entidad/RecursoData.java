package modelo.entidad;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import vistaModelo.RecursoFilter;
import modelo.entidad.Recurso;

public class RecursoData {

	private static List<Recurso> recursos = new ArrayList<Recurso>();
	static {
		recursos.add(new Recurso("001", "sillas", "22-10-12","sillas con buen soporte","21"));
		recursos.add(new Recurso("002", "mesas", "01-05-14","mesas de madera","5"));
		recursos.add(new Recurso("003", "enfriadores", "10-06-15","enfriador dos puertas","25"));
	
	}

	public static List<Recurso> getAllRecursos() {
		return new ArrayList<Recurso>(recursos);
	}
	

	// This Method only used in "Data Filter" Demo
	public static List<Recurso> getFilterRecursos(RecursoFilter recursoFilter) {
		List<Recurso> somerecursos = new ArrayList<Recurso>();
		String cod = recursoFilter.getCodigo().toLowerCase();
		String nom = recursoFilter.getNombre().toLowerCase();
		String fech = recursoFilter.getFechaingreso().toLowerCase();
		String can = recursoFilter.getCantidad().toLowerCase();
		String des = recursoFilter.getDescripcion().toLowerCase();

		for (Iterator<Recurso> i = recursos.iterator(); i.hasNext();) {
			Recurso tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getNombre().toLowerCase().contains(nom)  &&
					tmp.getFechaingreso().toLowerCase().contains(fech)  &&
					tmp.getCantidad().toLowerCase().contains(can)  &&
					tmp.getDescripcion().toLowerCase().contains(des)) {
				somerecursos.add(tmp);
			}
		}
		return somerecursos;
	}

	// This Method only used in "Header and footer" Demo
	public static List<Recurso> getRecursosByCodigo(String codigo) {
		List<Recurso> somerecursos = new ArrayList<Recurso>();
		for (Iterator<Recurso> i = recursos.iterator(); i.hasNext();) {
			Recurso tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				somerecursos.add(tmp);
			}
		}
		return somerecursos;
	}
}