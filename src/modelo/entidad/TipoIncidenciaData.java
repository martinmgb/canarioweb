package modelo.entidad;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import vistaModelo.TipoIncidenciaFilter;

public class TipoIncidenciaData {

	private static List<TipoIncidencia> incidencia = new ArrayList<TipoIncidencia>();
	static {
		incidencia.add(new TipoIncidencia("001", "TipoA", "XXXX"));
		incidencia.add(new TipoIncidencia("002", "TipoB", "XXXX"));
		incidencia.add(new TipoIncidencia("003", "TipoC", "XXXX"));
		
	}

	public static List<TipoIncidencia> getAllIncidencia() {
		return new ArrayList<TipoIncidencia>(incidencia);
	}
	public static TipoIncidencia[] getAllAreasArray() {
		return incidencia.toArray(new TipoIncidencia[incidencia.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<TipoIncidencia> getFilterIncidencia(TipoIncidenciaFilter incidenciaFilter) {
		List<TipoIncidencia> someareas = new ArrayList<TipoIncidencia>();
		String cod = incidenciaFilter.getCodigo().toLowerCase();
		String tip = incidenciaFilter.getTipo().toLowerCase();
		String des = incidenciaFilter.getDescripcion().toLowerCase();

		for (Iterator<TipoIncidencia> i = incidencia.iterator(); i.hasNext();) {
			TipoIncidencia tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getTipo().toLowerCase().contains(tip)  &&
					tmp.getDescripcion().toLowerCase().contains(des)) {
				someareas.add(tmp);
			}
		}
		return someareas;
	}

	// This Method only used in "Header and footer" Demo
	public static List<TipoIncidencia> getIncidenciasByCodigo(String codigo) {
		List<TipoIncidencia> someareas = new ArrayList<TipoIncidencia>();
		for (Iterator<TipoIncidencia> i = incidencia.iterator(); i.hasNext();) {
			TipoIncidencia tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				someareas.add(tmp);
			}
		}
		return someareas;
	}
}