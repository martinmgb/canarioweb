package modelo.entidad;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vistaModelo.TipoComisionFilter;
import vistaModelo.TipoComisionViewModel;
import modelo.entidad.TipoComision;

public class TipoComisionData {

	private static List<TipoComision> tipocomisiones = new ArrayList<TipoComision>();
	static {
		tipocomisiones.add(new TipoComision("001", "Tamarindo", "XXXX"));
		tipocomisiones.add(new TipoComision("002", "Sala de Fiestas", "XXXX"));
		tipocomisiones.add(new TipoComision("003", "Cancha de Bolas Criollas", "XXXX"));
		tipocomisiones.add(new TipoComision("004", "Tamarindo", "XXXX"));
		tipocomisiones.add(new TipoComision("005", "Piscina", "XXXX"));
		tipocomisiones.add(new TipoComision("006", "Tamarindo", "XXXX"));
		tipocomisiones.add(new TipoComision("007", "Piscina", "XXXX"));
		tipocomisiones.add(new TipoComision("008", "Tamarindo", "XXXX"));
		tipocomisiones.add(new TipoComision("009", "Piscina", "XXXX"));
		tipocomisiones.add(new TipoComision("010", "Cancha de Bolas Criollas", "XXXX"));
		tipocomisiones.add(new TipoComision("011", "Piscina", "XXXX"));
		tipocomisiones.add(new TipoComision("012", "Tamarindo", "XXXX"));
		tipocomisiones.add(new TipoComision("013", "Piscina", "XXXX"));
		tipocomisiones.add(new TipoComision("014", "Cancha de Bolas Criollas", "XXXX"));
		tipocomisiones.add(new TipoComision("015", "Piscina", "XXXX"));
		tipocomisiones.add(new TipoComision("016", "Tamarindo", "XXXX"));
		tipocomisiones.add(new TipoComision("017", "Piscina", "XXXX"));
		tipocomisiones.add(new TipoComision("018", "Tamarindo", "XXXX"));
		tipocomisiones.add(new TipoComision("019", "Piscina", "XXXX"));
		tipocomisiones.add(new TipoComision("020", "Piscina", "XXXX"));
		tipocomisiones.add(new TipoComision("021", "Sala de Fiestas", "XXXX"));
		tipocomisiones.add(new TipoComision("022", "Piscina", "XXXX"));
	}

	public static List<TipoComision> getAllTipoComisiones() {
		return new ArrayList<TipoComision>(tipocomisiones);
	}
	public static TipoComision[] getAllTipoComisionesArray() {
		return tipocomisiones.toArray(new TipoComision[tipocomisiones.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<TipoComision> getFilterTipoComisiones(TipoComisionFilter filtroTipoComision) {
		List<TipoComision> sometipocomisiones = new ArrayList<TipoComision>();
		String cod = filtroTipoComision.getCodigo().toLowerCase();
		String nom = filtroTipoComision.getNombre().toLowerCase();

		for (Iterator<TipoComision> i = tipocomisiones.iterator(); i.hasNext();) {
			TipoComision tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getNombre().toLowerCase().contains(nom)) {
				sometipocomisiones.add(tmp);
			}
		}
		return sometipocomisiones;
	}

	// This Method only used in "Header and footer" Demo
	public static List<TipoComision> getTipoComisionesByCodigo(String codigo) {
		List<TipoComision> sometipocomisiones = new ArrayList<TipoComision>();
		for (Iterator<TipoComision> i = tipocomisiones.iterator(); i.hasNext();) {
			TipoComision tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				sometipocomisiones.add(tmp);
			}
		}
		return sometipocomisiones;
	}
}