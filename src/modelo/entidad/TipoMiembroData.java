package modelo.entidad;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import vistaModelo.TipoMiembroFilter;
import modelo.entidad.TipoMiembro;

public class TipoMiembroData {

	private static List<TipoMiembro> miembros = new ArrayList<TipoMiembro>();
	static {
		miembros.add(new TipoMiembro("001", "Beneficiado"));
		miembros.add(new TipoMiembro("002", "Socio"));
		miembros.add(new TipoMiembro("003", "Afiliado"));
	}

	public static List<TipoMiembro> getAllMiembros() {
		return new ArrayList<TipoMiembro>(miembros);
	}
	public static TipoMiembro[] getAllMiembroArray() {
		return miembros.toArray(new TipoMiembro[miembros.size()]);
	}

		// This Method only used in "Data Filter" Demo
		public static List<TipoMiembro> getFilterMiembros(TipoMiembroFilter miembroFilter) {
			List<TipoMiembro> somemiembros = new ArrayList<TipoMiembro>();
			String cod = miembroFilter.getCodigo().toLowerCase();
			String tip = miembroFilter.getTipo().toLowerCase();
		
			for (Iterator<TipoMiembro> i = miembros.iterator(); i.hasNext();) {
				TipoMiembro tmp = i.next();
				if (tmp.getCodigo().toLowerCase().contains(cod) &&
						tmp.getTipo().toLowerCase().contains(tip)  ) {
					somemiembros.add(tmp);
				}
			}
			return somemiembros;
		}

		// This Method only used in "Header and footer" Demo
		public static List<TipoMiembro> getMiembrosByCodigo(String codigo) {
			List<TipoMiembro> somemiembros = new ArrayList<TipoMiembro>();
			for (Iterator<TipoMiembro> i = miembros.iterator(); i.hasNext();) {
				TipoMiembro tmp = i.next();
				if (tmp.getCodigo().equalsIgnoreCase(codigo)){
					somemiembros.add(tmp);
				}
			}
			return somemiembros;
		}
}