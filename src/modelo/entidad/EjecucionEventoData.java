package modelo.entidad;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import vistaModelo.EjecucionEventoFilter;
import modelo.entidad.EjecucionEvento;

public class EjecucionEventoData {

	private static List<EjecucionEvento> ejeevento = new ArrayList<EjecucionEvento>();
	static {
		ejeevento.add(new EjecucionEvento("",""));
		ejeevento.add(new EjecucionEvento("", ""));
		ejeevento.add(new EjecucionEvento("", ""));
	}

	public static List<EjecucionEvento> getAllEjecucionEventos() {
		return new ArrayList<EjecucionEvento>(ejeevento);
	}
	public static EjecucionEvento[] getAllCargoArray() {
		return ejeevento.toArray(new EjecucionEvento[ejeevento.size()]);
	}

		// This Method only used in "Data Filter" Demo
		public static List<EjecucionEvento> getFilterEjecucionEventos(EjecucionEventoFilter ejecucionEventoFilter) {
			List<EjecucionEvento> someejecucioneventos = new ArrayList<EjecucionEvento>();
			String cod = ejecucionEventoFilter.getCodigo().toLowerCase();
			String nom = ejecucionEventoFilter.getNombre().toLowerCase();
		
			for (Iterator<EjecucionEvento> i = ejeevento.iterator(); i.hasNext();) {
				EjecucionEvento tmp = i.next();
				if (tmp.getCodigo().toLowerCase().contains(cod) &&
						tmp.getNombre().toLowerCase().contains(nom)  ) {
					someejecucioneventos.add(tmp);
				}
			}
			return someejecucioneventos;
		}

		// This Method only used in "Header and footer" Demo
		public static List<EjecucionEvento> getCargosByCodigo(String codigo) {
			List<EjecucionEvento> someejecucioneventos = new ArrayList<EjecucionEvento>();
			for (Iterator<EjecucionEvento> i = ejeevento.iterator(); i.hasNext();) {
				EjecucionEvento tmp = i.next();
				if (tmp.getCodigo().equalsIgnoreCase(codigo)){
					someejecucioneventos.add(tmp);
				}
			}
			return someejecucioneventos;
		}
}