package modelo.entidad;

public class TipoMiembro {
	private String codigo;
	private String tipo;
	

	public TipoMiembro(String codigo, String tipo) {
		// TODO Auto-generated constructor stub
		this.codigo = codigo;
		this.tipo = tipo;
		
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	

}
