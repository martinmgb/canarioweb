package modelo.entidad;

import java.util.Date;

public class Incidencia {

	private String codigo;
	private String cedula;
	private String nombre;
	private String apellido;
	private String fecha;
	private String tipo;
	private String codigocaso;
	private String descripcion;

	public Incidencia(String codigo, String cedula, String nombre, String apellido, String fecha, String tipo, String codigocaso, String descripcion) {
		// TODO Auto-generated constructor stub
		this.codigo = codigo;
		this.cedula = cedula;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fecha = fecha;
		this.tipo = tipo;
		this.codigocaso = codigocaso;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public String getCodigocaso() {
		return codigocaso;
	}

	public void setCodigocaso(String codigocaso) {
		this.codigocaso = codigocaso;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	

}
