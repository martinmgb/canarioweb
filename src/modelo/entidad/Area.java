package modelo.entidad;

import java.io.Serializable;

public class Area implements  Serializable{
	  
	
	private static final long serialVersionUID = 1L;
	private boolean editingStatus=false;
	private char estatus= 'A';
	

	private int id;
	private String nombre;
	private String tipo;
	private  int capacidad;
	private float precio;
	private String condicion;
    private String imagen;
	
    public Area() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    public Area(int id, String nombre, String tipo, int capacidad,
			float precio, String condicion, String imagen) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.tipo = tipo;
		this.capacidad = capacidad;
		this.precio = precio;
		this.condicion = condicion;
		this.imagen = imagen;
	}

	public boolean isEditingStatus() {
		return editingStatus;
	}
	public void setEditingStatus(boolean editingStatus) {
		this.editingStatus = editingStatus;
	}
	public char getEstatus() {
		return estatus;
	}
	public void setEstatus(char estatus) {
		this.estatus = estatus;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public String getCondicion() {
		return condicion;
	}
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
     
     
	
}
