package modelo.entidad;
/*creado por Carlos Castillo 19454614
fecha 14/06/2015*/

public class ComisionPersona {
	private Comision comision;
	private Cliente cliente;
	
	public ComisionPersona(Comision comision, Cliente cliente) {
		super();
		this.comision = comision;
		this.cliente = cliente;
	}
	public ComisionPersona() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the comision
	 */
	public Comision getComision() {
		return comision;
	}
	/**
	 * @param comision the comision to set
	 */
	public void setComision(Comision comision) {
		this.comision = comision;
	}
	/**
	 * @return the cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}
	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	
}
