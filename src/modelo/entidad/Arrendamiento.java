package modelo.entidad;

import java.util.Date;

public class Arrendamiento {
	private String codigo;
	private String cedCliente;
	private String area;
	private Date fecha;
	

	public Arrendamiento(String codigo, String cedCliente, String area, Date fecha) 
	{
	// TODO Auto-generated constructor stub
		this.codigo = codigo;
		this.cedCliente = cedCliente;
		this.area = area;
		this.fecha = fecha;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getCedCliente() {
		return cedCliente;
	}


	public void setCedCliente(String cedCliente) {
		this.cedCliente = cedCliente;
	}


	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}


	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	
	
	
}
