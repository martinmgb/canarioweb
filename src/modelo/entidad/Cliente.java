package modelo.entidad;

public class Cliente {
	private String cedula;
	private String nombre;
	private String fechanacimiento;
	private String direccion;
	private String telefonof;
	private String telefonom;
	private String correo;
	private String twitter;
	private String facebook;
	public Cliente() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Cliente(String cedula, String nombre,String correo,String telefonom, String fechanacimiento,
			String direccion, String telefonof, 
			 String twitter, String facebook) {
		super();
		this.cedula = cedula;
		this.nombre = nombre;
		this.fechanacimiento = fechanacimiento;
		this.direccion = direccion;
		this.telefonof = telefonof;
		this.telefonom = telefonom;
		this.correo = correo;
		this.twitter = twitter;
		this.facebook = facebook;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFechanacimiento() {
		return fechanacimiento;
	}
	public void setFechanacimiento(String fechanacimiento) {
		this.fechanacimiento = fechanacimiento;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefonof() {
		return telefonof;
	}
	public void setTelefonof(String telefonof) {
		this.telefonof = telefonof;
	}
	public String getTelefonom() {
		return telefonom;
	}
	public void setTelefonom(String telefonom) {
		this.telefonom = telefonom;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getTwitter() {
		return twitter;
	}
	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}
	public String getFacebook() {
		return facebook;
	}
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	};

}
