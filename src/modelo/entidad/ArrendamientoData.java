package modelo.entidad;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import vistaModelo.ArrendamientoFilter;

public class ArrendamientoData {
	
	private static List<Arrendamiento> arrendamientos = new ArrayList<Arrendamiento>();
	
	static{
		
		arrendamientos.add(new Arrendamiento("001", "18861919", "Tamarindo", new Date("12/11/15")));
		arrendamientos.add(new Arrendamiento("002", "20188162", "Sala de fiestas", new Date("12/11/15")));
		arrendamientos.add(new Arrendamiento("003", "21456987", "Cancha de Bolas Crollas", new Date("12/11/15")));  
		arrendamientos.add(new Arrendamiento("004", "19869547", "Tamarindo", new Date("12/11/15")));   
		arrendamientos.add(new Arrendamiento("005", "17897562", "Tamarindo", new Date("12/11/15")));
		arrendamientos.add(new Arrendamiento("006", "7175378", "Piscina", new Date("12/11/15")));
		arrendamientos.add(new Arrendamiento("007", "7348682", "Tamarindo", new Date("12/11/15")));
		arrendamientos.add(new Arrendamiento("008", "13456875", "Sala de fiestas", new Date("12/11/15")));
		arrendamientos.add(new Arrendamiento("009", "14213659", "Piscina", new Date("12/11/15")));
		arrendamientos.add(new Arrendamiento("010", "18695321", "Tamarindo", new Date("12/11/15")));
		arrendamientos.add(new Arrendamiento("011", "14568147", "Tamarindo", new Date("12/11/15")));
		
	}
	public static List<Arrendamiento> getAllArrendamientos() {
		return new ArrayList<Arrendamiento>(arrendamientos);
	}
	public static Arrendamiento[] getAllArrendamientosArray() {
		return arrendamientos.toArray(new Arrendamiento[arrendamientos.size()]);
	}

	// This Method only used in "Data Filter" Demo
	public static List<Arrendamiento> getFilterArrendamientos(ArrendamientoFilter arrendamientoFilter) {
		List<Arrendamiento> somearrendamientos = new ArrayList<Arrendamiento>();
		String cod = arrendamientoFilter.getCodigo().toLowerCase();
		String cedCliente = arrendamientoFilter.getCedCliente().toLowerCase();
		String area = arrendamientoFilter.getArea().toLowerCase();
		//Date fecha = arrendamientoFilter.getFecha().toLowerCase();

		for (Iterator<Arrendamiento> i = arrendamientos.iterator(); i.hasNext();) {
			Arrendamiento tmp = i.next();
			if (tmp.getCodigo().toLowerCase().contains(cod) &&
					tmp.getCedCliente().toLowerCase().contains(cedCliente)  &&
					tmp.getArea().toLowerCase().contains(area) 
					//tmp.getFecha().toString().toLowerCase().contains(fecha.toString())
					) 
			{
				somearrendamientos.add(tmp);
			}
		}
		return somearrendamientos;
	}

	// This Method only used in "Header and footer" Demo
	public static List<Arrendamiento> getArrendamientosByCodigo(String codigo) {
		List<Arrendamiento> somearrendamientos = new ArrayList<Arrendamiento>();
		for (Iterator<Arrendamiento> i = arrendamientos.iterator(); i.hasNext();) {
			Arrendamiento tmp = i.next();
			if (tmp.getCodigo().equalsIgnoreCase(codigo)){
				somearrendamientos.add(tmp);
			}
		}
		return somearrendamientos;
	}
}

