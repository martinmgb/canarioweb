package modelo.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletContext;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Executions;

/**
 * Util class to upload files
 */

public class Util {

	public static final String separator = System.getProperty("file.separator");//Get de system separator

	public static boolean uploadFile(Media media) {
		return saveFile(media, getPath());

	}

	//Gets the path of the current web application
	public static String getPath(){
		return Executions.getCurrent().getDesktop().getWebApp().getRealPath(separator)+"uploads"+separator;
	}

	public static String getReal_Path(){
		return ((ServletContext)Executions.getCurrent().getSession().getWebApp()).getRealPath("/");
	}


	//save file
	public static boolean saveFile(Media media, String path){
		boolean uploaded = false;
		BufferedInputStream in = null;
		BufferedOutputStream out = null;
		try {
			String filePath =getPath();  

			InputStream ins = media.getStreamData();
			in = new BufferedInputStream(ins);

			String fileName = media.getName();

			System.out.println(filePath);
			File arc = new File("/home/fernando/workspace/Inmuebles/WebContent/uploads/" + fileName);
			//File arc = new File(filePath + fileName);
			OutputStream aout = new FileOutputStream(arc);
			out = new BufferedOutputStream(aout);
			byte buffer[] = new byte[1024];
			int ch = in.read(buffer);
			while(ch != -1){
				out.write(buffer, 0, ch);
				ch = in.read(buffer);
			}
			uploaded = true;
		}catch (IOException ie) {
			throw new RuntimeException(ie);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}finally{
			try {
				if(out != null)
					out.close();
				if(in != null)
					in.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		return uploaded;
	}
}
